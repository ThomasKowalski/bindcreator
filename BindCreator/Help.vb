﻿Public Class Help

    Private Sub lnkBlog_LinkClicked(sender As Object, e As LinkLabelLinkClickedEventArgs) Handles lnkBlog.LinkClicked
        Process.Start("http://thomaskowalski.net")
    End Sub

    Private Sub lnkTwitter_LinkClicked(sender As Object, e As LinkLabelLinkClickedEventArgs) Handles lnkTwitter.LinkClicked
        Process.Start("http://twitter.com/Pouknouki")
    End Sub

    Private Sub lnkContact_LinkClicked(sender As Object, e As LinkLabelLinkClickedEventArgs) Handles lnkContact.LinkClicked
        Process.Start("http://thomaskowalski.net/contact")
    End Sub

    Private Sub lnkMail_LinkClicked(sender As Object, e As LinkLabelLinkClickedEventArgs) Handles lnkMail.LinkClicked
        Process.Start("mailto:thomaskowalski@outlook.com")
    End Sub

    Private Sub lnkId_LinkClicked(sender As Object, e As LinkLabelLinkClickedEventArgs) Handles lnkId.LinkClicked
        Process.Start("http://steamcommunity.com/id/hoximor")
    End Sub

    Private Sub bTrade_Click(sender As Object, e As EventArgs) Handles bTrade.Click
        Process.Start("http://bit.ly/tradehoximor")
    End Sub

    Private Sub lnkGuide_LinkClicked(sender As Object, e As LinkLabelLinkClickedEventArgs) Handles lnkGuide.LinkClicked
        Process.Start("http://bit.ly/guidebindshoximor")
    End Sub
End Class