﻿Public Class Parametres

    Private Sub bValider_Click(sender As Object, e As EventArgs) Handles bValider.Click
        My.Computer.Registry.SetValue("HKEY_CURRENT_USER\Software\BindCreator\", "Path", tbCSGOPath.Text)
        WorkingDir = My.Computer.Registry.GetValue("HKEY_CURRENT_USER\Software\BindCreator\", "Path", "")
        My.Computer.Registry.SetValue("HKEY_CURRENT_USER\Software\BindCreator\", "AutomaticLoad", cbChargerAutomatiquement.Checked)
        Me.Close()
    End Sub

    Private Sub tbCSGOPath_TextChanged(sender As Object, e As EventArgs) Handles tbCSGOPath.TextChanged
        tbCSGOPath.Text = tbCSGOPath.Text.Replace("/", "\")
        If tbCSGOPath.Text = "" Then
            bValider.Enabled = False
            lblExplication.Text = My.Resources.provideherecsgofolder
        ElseIf My.Computer.FileSystem.DirectoryExists(tbCSGOPath.Text) = False Then
            bValider.Enabled = False
            lblExplication.Text = My.Resources.foldernotfound
        ElseIf tbCSGOPath.Text.Contains("csgo\cfg") And My.Computer.FileSystem.FileExists(tbCSGOPath.Text.Replace("csgo\cfg", "") & "\csgo.exe") Then
            bValider.Enabled = True
            tbCSGOPath.Text = tbCSGOPath.Text.Replace("\csgo\cfg", "")
            lblExplication.Text = My.Resources.foldercorrected
        ElseIf tbCSGOPath.Text.Contains("\csgo") And My.Computer.FileSystem.FileExists(tbCSGOPath.Text.Replace("\csgo", "") & "\csgo.exe") Then
            bValider.Enabled = True
            tbCSGOPath.Text = tbCSGOPath.Text.Replace("\csgo", "")
            lblExplication.Text = My.Resources.foldercorrected
        ElseIf My.Computer.FileSystem.FileExists(tbCSGOPath.Text & "\csgo.exe") = False Or My.Computer.FileSystem.DirectoryExists(tbCSGOPath.Text & "\csgo") = False Then
            bValider.Enabled = False
            lblExplication.Text = My.Resources.foldershouldcontain
        Else
            bValider.Enabled = True
            lblExplication.Text = My.Resources.directorycorrect
        End If
    End Sub

    Private Sub Parametres_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        If WorkingDir = "" Then
            WorkingDir = "C:\Program Files (x86)\Steam\SteamApps\common\Counter-Strike Global Offensive"
            tbCSGOPath.Text = "C:\Program Files (x86)\Steam\SteamApps\common\Counter-Strike Global Offensive"
        Else
            tbCSGOPath.Text = WorkingDir
        End If
        If tbCSGOPath.Text = "" Then
            bValider.Enabled = False
            lblExplication.Text = My.Resources.provideherecsgofolder
        ElseIf My.Computer.FileSystem.FileExists(tbCSGOPath.Text & "\csgo.exe") = False Or My.Computer.FileSystem.DirectoryExists(tbCSGOPath.Text & "\csgo") = False Then
            bValider.Enabled = False
            lblExplication.Text = My.Resources.foldershouldcontain
        Else
            bValider.Enabled = True
            lblExplication.Text = My.Resources.DirectoryCorrect
        End If
        cbChargerAutomatiquement.Checked = My.Computer.Registry.GetValue("HKEY_CURRENT_USER\Software\BindCreator\", "AutomaticLoad", "True")
    End Sub

    Private Sub bSelectCsgoExe_Click(sender As Object, e As EventArgs) Handles bSelectCsgoExe.Click
        With selectCSGO
            .CheckFileExists = True
            .Title = My.Resources.locatecsgoexe
            .Filter = My.Resources.csgoexecutable & "|csgo.exe"
            .ShowDialog()
        End With
        WorkingDir = selectCSGO.FileName.Replace("csgo.exe", "")
        tbCSGOPath.Text = WorkingDir
    End Sub
End Class