﻿Imports System.Resources

Imports System
Imports System.Reflection
Imports System.Runtime.InteropServices

' Les informations générales relatives à un assembly dépendent de 
' l'ensemble d'attributs suivant. Changez les valeurs de ces attributs pour modifier les informations
' associées à un assembly.

' Passez en revue les valeurs des attributs de l'assembly

<Assembly: AssemblyTitle("BindCreator")> 
<Assembly: AssemblyDescription("BindCreator vous permet de créer facilement des binds et des commandes pour Counter-Strike: Global Offensive.")> 
<Assembly: AssemblyCompany("Thomas Kowalski")> 
<Assembly: AssemblyProduct("BindCreator 2.1")> 
<Assembly: AssemblyCopyright("Copyright © Thomas Kowalski 2014")> 
<Assembly: AssemblyTrademark("Thomas Kowalski")> 

<Assembly: ComVisible(False)> 

'Le GUID suivant est pour l'ID de la typelib si ce projet est exposé à COM
<Assembly: Guid("a3e743f8-7bd0-45d4-be95-93926b1396b2")> 

' Les informations de version pour un assembly se composent des quatre valeurs suivantes :
'
'      Version principale
'      Version secondaire 
'      Numéro de build
'      Révision
'
' Vous pouvez spécifier toutes les valeurs ou indiquer les numéros de build et de révision par défaut 
' en utilisant '*', comme indiqué ci-dessous :
' <Assembly: AssemblyVersion("1.0.*")> 

<Assembly: AssemblyVersion("2.2.0.1")> 
<Assembly: AssemblyFileVersion("2.2.0.1")> 

<Assembly: NeutralResourcesLanguageAttribute("en")> 