﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Parametres
    Inherits System.Windows.Forms.Form

    'Form remplace la méthode Dispose pour nettoyer la liste des composants.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requise par le Concepteur Windows Form
    Private components As System.ComponentModel.IContainer

    'REMARQUE : la procédure suivante est requise par le Concepteur Windows Form
    'Elle peut être modifiée à l'aide du Concepteur Windows Form.  
    'Ne la modifiez pas à l'aide de l'éditeur de code.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(Parametres))
        Me.Label1 = New System.Windows.Forms.Label()
        Me.tbCSGOPath = New System.Windows.Forms.TextBox()
        Me.bValider = New System.Windows.Forms.Button()
        Me.lblExplication = New System.Windows.Forms.Label()
        Me.cbChargerAutomatiquement = New System.Windows.Forms.CheckBox()
        Me.bSelectCsgoExe = New System.Windows.Forms.Button()
        Me.selectCSGO = New System.Windows.Forms.OpenFileDialog()
        Me.SuspendLayout()
        '
        'Label1
        '
        resources.ApplyResources(Me.Label1, "Label1")
        Me.Label1.Name = "Label1"
        '
        'tbCSGOPath
        '
        resources.ApplyResources(Me.tbCSGOPath, "tbCSGOPath")
        Me.tbCSGOPath.Name = "tbCSGOPath"
        '
        'bValider
        '
        resources.ApplyResources(Me.bValider, "bValider")
        Me.bValider.Name = "bValider"
        Me.bValider.UseVisualStyleBackColor = True
        '
        'lblExplication
        '
        resources.ApplyResources(Me.lblExplication, "lblExplication")
        Me.lblExplication.Name = "lblExplication"
        '
        'cbChargerAutomatiquement
        '
        resources.ApplyResources(Me.cbChargerAutomatiquement, "cbChargerAutomatiquement")
        Me.cbChargerAutomatiquement.Name = "cbChargerAutomatiquement"
        Me.cbChargerAutomatiquement.UseVisualStyleBackColor = True
        '
        'bSelectCsgoExe
        '
        resources.ApplyResources(Me.bSelectCsgoExe, "bSelectCsgoExe")
        Me.bSelectCsgoExe.Name = "bSelectCsgoExe"
        Me.bSelectCsgoExe.UseVisualStyleBackColor = True
        '
        'selectCSGO
        '
        resources.ApplyResources(Me.selectCSGO, "selectCSGO")
        '
        'Parametres
        '
        resources.ApplyResources(Me, "$this")
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.Controls.Add(Me.bSelectCsgoExe)
        Me.Controls.Add(Me.cbChargerAutomatiquement)
        Me.Controls.Add(Me.lblExplication)
        Me.Controls.Add(Me.bValider)
        Me.Controls.Add(Me.tbCSGOPath)
        Me.Controls.Add(Me.Label1)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "Parametres"
        Me.ShowInTaskbar = False
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents tbCSGOPath As System.Windows.Forms.TextBox
    Friend WithEvents bValider As System.Windows.Forms.Button
    Friend WithEvents lblExplication As System.Windows.Forms.Label
    Friend WithEvents cbChargerAutomatiquement As System.Windows.Forms.CheckBox
    Friend WithEvents bSelectCsgoExe As System.Windows.Forms.Button
    Friend WithEvents selectCSGO As System.Windows.Forms.OpenFileDialog
End Class
