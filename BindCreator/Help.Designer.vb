﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Help
    Inherits System.Windows.Forms.Form

    'Form remplace la méthode Dispose pour nettoyer la liste des composants.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requise par le Concepteur Windows Form
    Private components As System.ComponentModel.IContainer

    'REMARQUE : la procédure suivante est requise par le Concepteur Windows Form
    'Elle peut être modifiée à l'aide du Concepteur Windows Form.  
    'Ne la modifiez pas à l'aide de l'éditeur de code.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(Help))
        Me.Label1 = New System.Windows.Forms.Label()
        Me.lnkTwitter = New System.Windows.Forms.LinkLabel()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.lnkBlog = New System.Windows.Forms.LinkLabel()
        Me.lnkGuide = New System.Windows.Forms.LinkLabel()
        Me.lnkContact = New System.Windows.Forms.LinkLabel()
        Me.lnkMail = New System.Windows.Forms.LinkLabel()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.lnkId = New System.Windows.Forms.LinkLabel()
        Me.bTrade = New System.Windows.Forms.Button()
        Me.SuspendLayout()
        '
        'Label1
        '
        resources.ApplyResources(Me.Label1, "Label1")
        Me.Label1.Name = "Label1"
        '
        'lnkTwitter
        '
        resources.ApplyResources(Me.lnkTwitter, "lnkTwitter")
        Me.lnkTwitter.Name = "lnkTwitter"
        Me.lnkTwitter.TabStop = True
        '
        'Label2
        '
        resources.ApplyResources(Me.Label2, "Label2")
        Me.Label2.Name = "Label2"
        '
        'Label3
        '
        resources.ApplyResources(Me.Label3, "Label3")
        Me.Label3.Name = "Label3"
        '
        'Label4
        '
        resources.ApplyResources(Me.Label4, "Label4")
        Me.Label4.Name = "Label4"
        '
        'Label5
        '
        resources.ApplyResources(Me.Label5, "Label5")
        Me.Label5.Name = "Label5"
        '
        'Label6
        '
        resources.ApplyResources(Me.Label6, "Label6")
        Me.Label6.Name = "Label6"
        '
        'lnkBlog
        '
        resources.ApplyResources(Me.lnkBlog, "lnkBlog")
        Me.lnkBlog.Name = "lnkBlog"
        Me.lnkBlog.TabStop = True
        '
        'lnkGuide
        '
        resources.ApplyResources(Me.lnkGuide, "lnkGuide")
        Me.lnkGuide.Name = "lnkGuide"
        Me.lnkGuide.TabStop = True
        '
        'lnkContact
        '
        resources.ApplyResources(Me.lnkContact, "lnkContact")
        Me.lnkContact.Name = "lnkContact"
        Me.lnkContact.TabStop = True
        '
        'lnkMail
        '
        resources.ApplyResources(Me.lnkMail, "lnkMail")
        Me.lnkMail.Name = "lnkMail"
        Me.lnkMail.TabStop = True
        '
        'Label7
        '
        resources.ApplyResources(Me.Label7, "Label7")
        Me.Label7.Name = "Label7"
        '
        'lnkId
        '
        resources.ApplyResources(Me.lnkId, "lnkId")
        Me.lnkId.Name = "lnkId"
        Me.lnkId.TabStop = True
        '
        'bTrade
        '
        resources.ApplyResources(Me.bTrade, "bTrade")
        Me.bTrade.Name = "bTrade"
        Me.bTrade.UseVisualStyleBackColor = True
        '
        'Help
        '
        resources.ApplyResources(Me, "$this")
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.Controls.Add(Me.bTrade)
        Me.Controls.Add(Me.Label7)
        Me.Controls.Add(Me.Label6)
        Me.Controls.Add(Me.Label5)
        Me.Controls.Add(Me.Label4)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.lnkGuide)
        Me.Controls.Add(Me.lnkBlog)
        Me.Controls.Add(Me.lnkId)
        Me.Controls.Add(Me.lnkMail)
        Me.Controls.Add(Me.lnkContact)
        Me.Controls.Add(Me.lnkTwitter)
        Me.Controls.Add(Me.Label1)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow
        Me.Name = "Help"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents lnkTwitter As System.Windows.Forms.LinkLabel
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents lnkBlog As System.Windows.Forms.LinkLabel
    Friend WithEvents lnkGuide As System.Windows.Forms.LinkLabel
    Friend WithEvents lnkContact As System.Windows.Forms.LinkLabel
    Friend WithEvents lnkMail As System.Windows.Forms.LinkLabel
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents lnkId As System.Windows.Forms.LinkLabel
    Friend WithEvents bTrade As System.Windows.Forms.Button
End Class
