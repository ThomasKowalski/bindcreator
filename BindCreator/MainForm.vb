﻿Imports System.IO
Imports System.Net
Imports System.Resources

Public Class MainForm
#Region "Misc"
    Private Sub MainForm_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        WorkingDir = My.Computer.Registry.GetValue("HKEY_CURRENT_USER\Software\BindCreator\", "Path", "")
        If WorkingDir = "" Then
            Parametres.ShowDialog()
        ElseIf My.Computer.FileSystem.DirectoryExists(WorkingDir) = False Then
            Parametres.ShowDialog()
        ElseIf My.Computer.FileSystem.FileExists(WorkingDir & "\csgo.exe") = False Or My.Computer.FileSystem.DirectoryExists(WorkingDir & "\csgo") = False Then
            Parametres.ShowDialog()
        Else
            Parametres.tbCSGOPath.Text = My.Computer.Registry.GetValue("HKEY_CURRENT_USER\Software\BindCreator\", "Path", "")
        End If
        If My.Computer.Registry.GetValue("HKEY_CURRENT_USER\Software\BindCreator\", "AutomaticLoad", "True") Then
            ImporterCSGO()
        End If
        workerUpdates.RunWorkerAsync()
    End Sub
    Private Function URLEncode(parameter As String) As String
        Return System.Uri.EscapeDataString(parameter)
    End Function
    Private Sub workerUpdates_DoWork(sender As Object, e As System.ComponentModel.DoWorkEventArgs) Handles workerUpdates.DoWork
        Dim webClient As New WebClient
        Dim lastVersion As String = webClient.DownloadString("http://thomaskowalski.net/versions/bindcreator")
        If lastVersion <> Application.ProductVersion Then
            Dim Download As DialogResult = MessageBox.Show(My.Resources.NewVersionFound, My.Resources.Update, MessageBoxButtons.YesNo, MessageBoxIcon.Information)
            If Download = Windows.Forms.DialogResult.Yes Then
                Dim process As New Process
                process.Start("https://bitbucket.org/ThomasKowalski/bindcreator/raw/master/BindCreator/bin/Debug/BindCreator.zip")
            End If
        End If
    End Sub
#End Region
#Region "Weapons"
    Private Sub lnkAide_LinkClicked(sender As Object, e As LinkLabelLinkClickedEventArgs) Handles lnkAide.LinkClicked
        Help.Show()
        Help.BringToFront()
    End Sub
    Sub Process()
        If FirstClick <> 0 Then
            bBack.Enabled = True
        Else
            bBack.Enabled = False
        End If
        If SecondClick <> 0 Then
            For i = 0 To listWeapons.Items.Count - 1
                If listWeapons.Items(i) = TableauArmes(FirstClick & SecondClick) Then
                    FirstClick = 0
                    SecondClick = 0
                    AfficherArmes()
                    Exit Sub
                End If
            Next
            If TableauArmes(FirstClick & SecondClick) = "" Then
                FirstClick = FirstClick
                SecondClick = 0
                AfficherArmes()
                Exit Sub
            End If
            listWeapons.Items.Add(TableauArmes(FirstClick & SecondClick))
            lblNombreAchats.Text = My.Resources.Buys & " : " & listWeapons.Items.Count
            CurrentCommand += TableauEquivalents(FirstClick & SecondClick) & " "
            If FirstClick <> 6 Then
                FirstClick = 0
            End If
            SecondClick = 0
        End If
        If ToucheActuelle = "" Then
            lblStatus.Text = My.Resources.PleaseSelectAKey
        ElseIf tbResultat.Text.Contains("bind " & ToucheActuelle) Then
            lblStatus.Text = My.Resources.AlreadyAssigned
        ElseIf listWeapons.Items.Count = 0 Then
            lblStatus.Text = My.Resources.PleaseAddABuy
        ElseIf listWeapons.Items.Count > 0 And ToucheActuelle <> "" Then
            lblStatus.Text = My.Resources.Ready
        End If
        Dim PrixTotal As Integer = 0
        lblNom.Text = My.Resources.NameUndefined
        lblPrix.Text = My.Resources.priceundefined
        picturePreview.Image = Nothing
        If listWeapons.Items.Count > 0 Then
            For i = 0 To listWeapons.Items.Count - 1
                For j = 0 To TableauArmes.Length - 1
                    If listWeapons.Items(i) = TableauArmes(j) Then
                        PrixTotal += TableauPrix(j)
                        Exit For
                    End If
                Next
            Next
        End If
        lblPrix.Text = My.Resources.totalprice & PrixTotal
        AfficherArmes()
        tbResultat.Text = tbResultat.Text.Replace(vbCrLf & vbCrLf, vbCrLf)
        tbResultat.Text = tbResultat.Text.Replace("""bind", """" & vbCrLf & "bind")
        tbResultat.Text = tbResultat.Text.Replace(";b", "; b")
        '    tbResultat.Text = tbResultat.Text.Replace(vbLf, "")
    End Sub
    Sub AfficherArmes()
        b5.Enabled = True
        b6.Enabled = True
        Select Case FirstClick
            Case 0
                b1.Text = My.Resources.pistols
                b2.Text = My.Resources.heavy
                b3.Text = My.Resources.smg
                b4.Text = My.Resources.rifles
                b5.Text = My.Resources.equipment
                b6.Text = My.Resources.grenades
            Case 1
                b1.Text = TableauArmes(11)
                b2.Text = TableauArmes(12)
                b3.Text = TableauArmes(13)
                b4.Text = TableauArmes(14)
                b5.Text = TableauArmes(15)
                b6.Text = "∅"
                b6.Enabled = False
            Case 2
                b1.Text = TableauArmes(21)
                b2.Text = TableauArmes(22)
                b3.Text = TableauArmes(23)
                b4.Text = TableauArmes(24)
                b5.Text = TableauArmes(25)
                b6.Text = "∅"
                b6.Enabled = False
            Case 3
                b1.Text = TableauArmes(31)
                b2.Text = TableauArmes(32)
                b3.Text = TableauArmes(33)
                b4.Text = TableauArmes(34)
                b5.Text = TableauArmes(35)
                b6.Text = "∅"
                b6.Enabled = False
            Case 4
                b1.Text = TableauArmes(41)
                b2.Text = TableauArmes(42)
                b3.Text = TableauArmes(43)
                b4.Text = TableauArmes(44)
                b5.Text = TableauArmes(45)
                b6.Text = TableauArmes(46)
            Case 5
                b1.Text = TableauArmes(51)
                b2.Text = TableauArmes(52)
                b3.Text = TableauArmes(53)
                b4.Text = TableauArmes(54)
                b5.Text = "∅"
                b6.Text = "∅"
                b5.Enabled = False
                b6.Enabled = False
            Case 6
                b1.Text = TableauArmes(61)
                b2.Text = TableauArmes(62)
                b3.Text = TableauArmes(63)
                b4.Text = TableauArmes(64)
                b5.Text = TableauArmes(65)
                b6.Text = "∅"
                b6.Enabled = False
        End Select
    End Sub
    Sub AfficherPrix(Prix As Double)
        lblPrix.Text = My.Resources.price & Prix
    End Sub
    Sub AfficherNom(Nom As String)
        lblNom.Text = My.Resources.name & Nom
    End Sub

    'Clics sur les boutons des armes et hover
    Private Sub b1_Click(sender As Object, e As EventArgs) Handles b1.Click
        If FirstClick = 0 Then
            FirstClick = 1
        ElseIf SecondClick = 0 Then
            SecondClick = 1
        End If
        Process()
    End Sub
    Private Sub b2_Click(sender As Object, e As EventArgs) Handles b2.Click
        If FirstClick = 0 Then
            FirstClick = 2
        ElseIf SecondClick = 0 Then
            SecondClick = 2
        End If
        Process()
    End Sub
    Private Sub b3_Click(sender As Object, e As EventArgs) Handles b3.Click
        If FirstClick = 0 Then
            FirstClick = 3
        ElseIf SecondClick = 0 Then
            SecondClick = 3
        End If
        Process()
    End Sub
    Private Sub b4_Click(sender As Object, e As EventArgs) Handles b4.Click
        If FirstClick = 0 Then
            FirstClick = 4
        ElseIf SecondClick = 0 Then
            SecondClick = 4
        End If
        Process()

    End Sub
    Private Sub b5_Click(sender As Object, e As EventArgs) Handles b5.Click
        If FirstClick = 0 Then
            FirstClick = 5
        ElseIf SecondClick = 0 Then
            SecondClick = 5
        End If
        Process()

    End Sub
    Private Sub b6_Click(sender As Object, e As EventArgs) Handles b6.Click
        If FirstClick = 0 Then
            FirstClick = 6
        ElseIf SecondClick = 0 Then
            SecondClick = 6
        End If
        Process()

    End Sub
    Private Sub b1_MouseEnter(sender As Object, e As EventArgs) Handles b1.MouseEnter, b1.MouseClick
        If FirstClick <> 0 Then
            AfficherNom(TableauArmes(FirstClick & 1))
            AfficherPrix(TableauPrix(FirstClick & 1))
            picturePreview.Image = TableauImages(FirstClick & 1)
        End If
    End Sub
    Private Sub b2_MouseEnter(sender As Object, e As EventArgs) Handles b2.MouseEnter, b2.MouseClick
        If FirstClick <> 0 Then
            AfficherNom(TableauArmes(FirstClick & 2))
            AfficherPrix(TableauPrix(FirstClick & 2))
            picturePreview.Image = TableauImages(FirstClick & 2)
        End If
    End Sub
    Private Sub b3_MouseEnter(sender As Object, e As EventArgs) Handles b3.MouseEnter, b3.MouseClick
        If FirstClick <> 0 Then
            AfficherNom(TableauArmes(FirstClick & 3))
            AfficherPrix(TableauPrix(FirstClick & 3))
            picturePreview.Image = TableauImages(FirstClick & 3)
        End If
    End Sub
    Private Sub b4_MouseEnter(sender As Object, e As EventArgs) Handles b4.MouseEnter, b4.MouseClick
        If FirstClick <> 0 Then
            AfficherNom(TableauArmes(FirstClick & 4))
            AfficherPrix(TableauPrix(FirstClick & 4))
            picturePreview.Image = TableauImages(FirstClick & 4)
        End If
    End Sub
    Private Sub b5_MouseEnter(sender As Object, e As EventArgs) Handles b5.MouseEnter, b5.MouseClick
        If FirstClick <> 0 Then
            AfficherNom(TableauArmes(FirstClick & 5))
            AfficherPrix(TableauPrix(FirstClick & 5))
            picturePreview.Image = TableauImages(FirstClick & 5)
        End If
    End Sub
    Private Sub b6_MouseEnter(sender As Object, e As EventArgs) Handles b6.MouseEnter, b6.MouseClick
        If FirstClick <> 0 Then
            AfficherNom(TableauArmes(FirstClick & 6))
            AfficherPrix(TableauPrix(FirstClick & 6))
            picturePreview.Image = TableauImages(FirstClick & 6)
        End If
    End Sub
    Private Sub b1_MouseLeave(sender As Object, e As EventArgs) Handles b6.MouseLeave, b2.MouseLeave, b3.MouseLeave, b1.MouseLeave, b4.MouseLeave, b5.MouseLeave
        Process()
    End Sub

    Private Sub bBack_Click(sender As Object, e As EventArgs) Handles bBack.Click
        FirstClick = 0
        SecondClick = 0
        Process()
    End Sub
    Private Sub bValider_Click(sender As Object, e As EventArgs) Handles bValider.Click
        If ToucheActuelle = "" Then
            MessageBox.Show(My.Resources.PleaseSelectAKeyInKeyboardWindow, My.Resources.nokeyselected, MessageBoxButtons.OK, MessageBoxIcon.Error)
            Exit Sub
        ElseIf listWeapons.Items.Count = 0 Then
            MessageBox.Show(My.Resources.PleaseAddAtLeastOneBuy, My.Resources.nothingtobuy, MessageBoxButtons.OK, MessageBoxIcon.Error)
            Exit Sub
        End If
        If tbResultat.Text.Contains("bind " & ToucheActuelle) Then
            Dim TableauResultat As String() = tbResultat.Text.Split(vbCrLf)
            Dim MB = MessageBox.Show(My.Resources.keyalreadyassignedmb, My.Resources.keyalreadyassignedmbtitle, MessageBoxButtons.YesNo)
            If MB = Windows.Forms.DialogResult.Yes Then
                For i = 0 To TableauResultat.Length - 1
                    If TableauResultat(i).Replace(vbCrLf, "").Contains("bind " & ToucheActuelle) Then
                        TableauResultat(i) = "bind " & ToucheActuelle & " " & """" & CurrentCommand.Substring(0, CurrentCommand.Length - 1) & """"
                        Exit For
                    End If
                Next
            Else
                Exit Sub
            End If
            tbResultat.Text = ""
            For i = 0 To TableauResultat.Length - 1
                tbResultat.Text += TableauResultat(i) & vbCrLf
            Next
        Else
            tbResultat.Text += "bind " & ToucheActuelle & " " & """" & CurrentCommand.Substring(0, CurrentCommand.Length - 1) & """" & vbCrLf
        End If
        CurrentCommand = ""
        ToucheActuelle = ""
        lblTouche.Text = "Touche : ND"
        listWeapons.Items.Clear()
        lblNombreAchats.Text = "Achats : " & listWeapons.Items.Count
        lblPrix.Text = "Prix total $0"
        tbResultat.Text = tbResultat.Text.Replace(vbCrLf & vbCrLf, vbCrLf)
        tbResultat.Text = tbResultat.Text.Replace("""bind", """" & vbCrLf & "bind")
        Process()
    End Sub

    Private Sub lblSupprimer_LinkClicked(sender As Object, e As LinkLabelLinkClickedEventArgs) Handles lblSupprimer.LinkClicked
        If listWeapons.SelectedItem = "" Then
            Exit Sub
        End If
        Dim indice As Integer = 0
        For i = 0 To TableauArmes.Length - 1
            If listWeapons.SelectedItem = TableauArmes(i) Then
                indice = i
                Exit For
            End If
        Next
        CurrentCommand = CurrentCommand.Replace(TableauEquivalents(indice), "")
        listWeapons.Items.Remove(listWeapons.SelectedItem)
        lblNombreAchats.Text = My.Resources.Buys & listWeapons.Items.Count
        Process()
    End Sub

    Private Sub Weapons_KeyDown(sender As Object, e As KeyEventArgs) Handles Me.KeyDown, b1.KeyDown, b2.KeyDown, b3.KeyDown, b4.KeyDown, b5.KeyDown, b6.KeyDown, bBack.KeyDown, bOptions.KeyDown, bSwapTeam.KeyDown, bValider.KeyDown, cEquivalents.KeyDown
        Dim Touche As Double = 0
        If e.KeyCode = Keys.NumPad1 Then 'si la touche controle est appuyée ainsi que la touche S
            Touche = 1
        ElseIf e.KeyCode = Keys.NumPad2 Then
            Touche = 2
        ElseIf e.KeyCode = Keys.NumPad3 Then
            Touche = 3
        ElseIf e.KeyCode = Keys.NumPad4 Then
            Touche = 4
        ElseIf e.KeyCode = Keys.NumPad5 Then
            Touche = 5
        ElseIf e.KeyCode = Keys.NumPad6 Then
            Touche = 6
        ElseIf e.KeyCode = Keys.NumPad7 Then
            'Touche = 7
        ElseIf e.KeyCode = Keys.NumPad8 Then
            'Touche = 8
        ElseIf e.KeyCode = Keys.NumPad9 Then
            'Touche = 9
        ElseIf e.KeyCode = Keys.Escape Then
            FirstClick = 0
            SecondClick = 0
            Process()
        End If
        If FirstClick = 0 Then
            FirstClick = Touche
        ElseIf SecondClick = 0 Then
            SecondClick = Touche
        End If
        Process()
    End Sub

    Private Sub lblTouche_TextChanged(sender As Object, e As EventArgs) Handles lblTouche.TextChanged
        Process()
    End Sub

    Private Sub bSwapTeam_Click(sender As Object, e As EventArgs) Handles bSwapTeam.Click
        If Team = 1 Then
            Team = 0
            bSwapTeam.Text = My.Resources.TeamCT
            TableauArmes = TableauArmesCT
            TableauImages = TableauImagesCT
            TableauPrix = TableauPrixCounter
            If cEquivalents.Checked Then
                TableauEquivalents = TableauEquivalentsFusionnes
            Else
                TableauEquivalents = TableauEquivalentsCT
            End If
        Else
            Team = 1
            bSwapTeam.Text = My.Resources.TeamT
            TableauArmes = TableauArmesT
            TableauImages = TableauImagesT
            TableauPrix = TableauPrixTerro
            If cEquivalents.Checked Then
                TableauEquivalents = TableauEquivalentsFusionnes
            Else
                TableauEquivalents = TableauEquivalentsT
            End If
        End If
        Process()
    End Sub

    Private Sub cEquivalents_CheckedChanged(sender As Object, e As EventArgs) Handles cEquivalents.CheckedChanged
        If Team = 0 Then
            If cEquivalents.Checked Then
                TableauEquivalents = TableauEquivalentsFusionnes
            Else
                TableauEquivalents = TableauEquivalentsCT
            End If
        Else
            If cEquivalents.Checked Then
                TableauEquivalents = TableauEquivalentsFusionnes
            Else
                TableauEquivalents = TableauEquivalentsT
            End If
        End If
    End Sub

    Private Sub bOptions_Click(sender As Object, e As EventArgs) Handles bOptions.Click
        Parametres.ShowDialog()
    End Sub

    Private Sub listWeapons_KeyDown(sender As Object, e As KeyEventArgs) Handles listWeapons.KeyDown
        If e.KeyCode = Keys.Delete Then
            If listWeapons.SelectedItem = "" Then
                Exit Sub
            End If
            Dim indice As Integer = 0
            For i = 0 To TableauArmes.Length - 1
                If listWeapons.SelectedItem = TableauArmes(i) Then
                    indice = i
                    Exit For
                End If
            Next
            CurrentCommand = CurrentCommand.Replace(TableauEquivalents(indice), "")
            listWeapons.Items.Remove(listWeapons.SelectedItem)
            lblNombreAchats.Text = "Achats : " & listWeapons.Items.Count
        End If
        Process()
    End Sub
#End Region
#Region "Result"
    Private Sub bSaveFile_Click(sender As Object, e As EventArgs) Handles bSaveFile.Click
        With dialogSave
            .AddExtension = True
            .Filter = My.Resources.sourceconfigfile & "|*.cfg"
            .DefaultExt = ".cfg"
            .InitialDirectory = WorkingDir
            .Title = My.Resources.savebindsfile
            .ShowDialog()
        End With
        If dialogSave.FileName = "" Then
            Exit Sub
        End If
        Try
            My.Computer.FileSystem.DeleteFile(dialogSave.FileName)
        Catch ex As Exception

        End Try
        Dim Ecriveur As New StreamWriter(dialogSave.FileName, False)
        Ecriveur.Write(tbResultat.Text)
        Ecriveur.Close()
    End Sub

    Private Sub tbImporter_Click(sender As Object, e As EventArgs) Handles tbImporter.Click
        ImporterCSGO()
    End Sub

    Public Sub ImporterCSGO()
        Dim Lecteur
        If My.Computer.FileSystem.FileExists(WorkingDir & "\csgo\cfg\binds.cfg") Then
            Lecteur = New StreamReader(WorkingDir & "\csgo\cfg\binds.cfg", System.Text.Encoding.Default)
            tbResultat.Text = Lecteur.ReadToEnd
            Lecteur.Close()
            Lecteur = Nothing
            Dim Writer As New StreamWriter(WorkingDir & "\csgo\cfg\autoexec.cfg")
            Writer.Write(tbResultat.Text)
            Writer.Close()
            My.Computer.FileSystem.DeleteFile(WorkingDir & "\csgo\cfg\binds.cfg")
        End If
        If My.Computer.FileSystem.FileExists(WorkingDir & "\csgo\cfg\autoexec.cfg") = False Then
            MessageBox.Show(My.Resources.CouldntImportFileNotFound, My.Resources.ErrorHappened, MessageBoxButtons.OK, MessageBoxIcon.Error)
            Exit Sub
        End If
        Lecteur = New StreamReader(WorkingDir & "\csgo\cfg\autoexec.cfg", System.Text.Encoding.Default)
        tbResultat.Text = Lecteur.ReadToEnd
        Lecteur.Close()
        Lecteur = Nothing
    End Sub

    Private Sub tbExporter_Click(sender As Object, e As EventArgs) Handles tbExporter.Click
        If My.Computer.FileSystem.FileExists(WorkingDir & "\csgo\cfg\autoexec.cfg") Then
            Dim LULZ = MessageBox.Show(My.Resources.FileExistsOverwrite, My.Resources.Overwrite, MessageBoxButtons.YesNo, MessageBoxIcon.Question)
            If LULZ = Windows.Forms.DialogResult.Yes Then
            Else
                Exit Sub
            End If
        End If
        Dim Writer As New StreamWriter(WorkingDir & "\csgo\cfg\autoexec.cfg", False)
        Writer.Write(tbResultat.Text)
        Writer.Close()
    End Sub

    Private Sub tbResultat_TextChanged(sender As Object, e As EventArgs) Handles tbResultat.TextChanged
        ParseLines()
    End Sub

    Private Sub bCharger_Click(sender As Object, e As EventArgs) Handles bCharger.Click
        With dialogOuvrir
            .AddExtension = True
            .Filter = My.Resources.sourceconfigfile & "|*.cfg"
            .DefaultExt = ".cfg"
            .InitialDirectory = WorkingDir
            .Title = My.Resources.openbindsfile
            .ShowDialog()
        End With
        If dialogOuvrir.FileName = "" Or My.Computer.FileSystem.FileExists(dialogOuvrir.FileName) = False Then
            Exit Sub
        End If
        Dim Lecteur As New StreamReader(dialogOuvrir.FileName, System.Text.Encoding.Default)
        tbResultat.Text = Lecteur.ReadToEnd
        Lecteur.Close()
    End Sub
#End Region
#Region "Keyboard"
    Sub VerifierTouche()
        Process()
    End Sub
    Private Sub bIns_Click(sender As Object, e As EventArgs) Handles bIns.Click
        lblTouche.Text = My.Resources.key & My.Resources.insert
        ToucheActuelle = "ins"
        VerifierTouche()
    End Sub

    Private Sub bHome_Click(sender As Object, e As EventArgs) Handles bHome.Click
        lblTouche.Text = My.Resources.key & My.Resources.home
        ToucheActuelle = "home"
        VerifierTouche()
    End Sub

    Private Sub bPgUp_Click(sender As Object, e As EventArgs) Handles bPgUp.Click
        lblTouche.Text = My.Resources.key & My.Resources.pageup
        ToucheActuelle = "pgup"
        VerifierTouche()
    End Sub

    Private Sub bDel_Click(sender As Object, e As EventArgs) Handles bDel.Click
        lblTouche.Text = My.Resources.key & My.Resources.delete
        ToucheActuelle = "del"
        VerifierTouche()
    End Sub

    Private Sub bEnd_Click(sender As Object, e As EventArgs) Handles bEnd.Click
        lblTouche.Text = My.Resources.Key & My.Resources.EndKey
        ToucheActuelle = "end"
        VerifierTouche()
    End Sub

    Private Sub bPgDown_Click(sender As Object, e As EventArgs) Handles bPgDown.Click
        lblTouche.Text = My.Resources.key & My.Resources.pagedown
        ToucheActuelle = "pgdown"
        VerifierTouche()
    End Sub

    Private Sub bUpArrow_Click(sender As Object, e As EventArgs) Handles bUpArrow.Click
        lblTouche.Text = My.Resources.key & My.Resources.up
        ToucheActuelle = "uparrow"
        VerifierTouche()
    End Sub

    Private Sub bLeftArrow_Click(sender As Object, e As EventArgs) Handles bLeftArrow.Click
        lblTouche.Text = My.Resources.key & My.Resources.left
        ToucheActuelle = "leftarrow"
        VerifierTouche()
    End Sub

    Private Sub bDownArrow_Click(sender As Object, e As EventArgs) Handles bDownArrow.Click
        lblTouche.Text = My.Resources.key & My.Resources.down
        ToucheActuelle = "downarrow"
        VerifierTouche()
    End Sub

    Private Sub bRightArrow_Click(sender As Object, e As EventArgs) Handles bRightArrow.Click
        lblTouche.Text = My.Resources.key & My.Resources.right
        ToucheActuelle = "rightarrow"
        VerifierTouche()
    End Sub

    Private Sub bKP_Slash_Click(sender As Object, e As EventArgs) Handles bKP_Slash.Click
        lblTouche.Text = my.resources.key & "/"
        ToucheActuelle = "kp_slash"
        VerifierTouche()
    End Sub

    Private Sub bKP_Multiply_Click(sender As Object, e As EventArgs) Handles bKP_Multiply.Click
        lblTouche.Text = my.resources.key & "*"
        ToucheActuelle = "kp_multiply"
        VerifierTouche()
    End Sub

    Private Sub bKP_Minus_Click(sender As Object, e As EventArgs) Handles bKP_Minus.Click
        lblTouche.Text = my.resources.key & "-"
        ToucheActuelle = "kp_minus"
        VerifierTouche()
    End Sub

    Private Sub bKP_Home_Click(sender As Object, e As EventArgs) Handles bKP_Home.Click
        lblTouche.Text = my.resources.key & "7"
        ToucheActuelle = "kp_home"
        VerifierTouche()
    End Sub

    Private Sub bKP_Up_Click(sender As Object, e As EventArgs) Handles bKP_Up.Click
        lblTouche.Text = my.resources.key & "8"
        ToucheActuelle = "kp_uparrow"
        VerifierTouche()
    End Sub

    Private Sub bKP_PgUp_Click(sender As Object, e As EventArgs) Handles bKP_PgUp.Click
        lblTouche.Text = my.resources.key & "9"
        ToucheActuelle = "kp_pgup"
        VerifierTouche()
    End Sub

    Private Sub bKP_Left_Click(sender As Object, e As EventArgs) Handles bKP_Left.Click
        lblTouche.Text = my.resources.key & "4"
        ToucheActuelle = "kp_leftarrow"
        VerifierTouche()
    End Sub

    Private Sub bKP_5_Click(sender As Object, e As EventArgs) Handles bKP_5.Click
        lblTouche.Text = my.resources.key & "5"
        ToucheActuelle = "kp_5"
        VerifierTouche()
    End Sub

    Private Sub bKP_Right_Click(sender As Object, e As EventArgs) Handles bKP_Right.Click
        lblTouche.Text = my.resources.key & "6"
        ToucheActuelle = "kp_rightarrow"
        VerifierTouche()
    End Sub

    Private Sub bKP_Plus_Click(sender As Object, e As EventArgs) Handles bKP_Plus.Click
        lblTouche.Text = my.resources.key & "+"
        ToucheActuelle = "kp_plus"
        VerifierTouche()
    End Sub

    Private Sub bKP_End_Click(sender As Object, e As EventArgs) Handles bKP_End.Click
        lblTouche.Text = my.resources.key & "1"
        ToucheActuelle = "kp_end"
        VerifierTouche()
    End Sub

    Private Sub bKP_Down_Click(sender As Object, e As EventArgs) Handles bKP_Down.Click
        lblTouche.Text = my.resources.key & "2"
        ToucheActuelle = "kp_downarrow"
        VerifierTouche()
    End Sub

    Private Sub bKP_PgDown_Click(sender As Object, e As EventArgs) Handles bKP_PgDown.Click
        lblTouche.Text = my.resources.key & "3"
        ToucheActuelle = "kp_pgdown"
        VerifierTouche()
    End Sub

    Private Sub bKP_Ins_Click(sender As Object, e As EventArgs) Handles bKP_Ins.Click
        lblTouche.Text = my.resources.key & "0"
        ToucheActuelle = "kp_ins"
        VerifierTouche()
    End Sub

    Private Sub bKP_Del_Click(sender As Object, e As EventArgs) Handles bKP_Del.Click
        lblTouche.Text = my.resources.key & "."
        ToucheActuelle = "kp_del"
        VerifierTouche()
    End Sub

    Private Sub bKP_Enter_Click(sender As Object, e As EventArgs) Handles bKP_Enter.Click
        lblTouche.Text = my.resources.key & "Enter"
        ToucheActuelle = "kp_enter"
        VerifierTouche()
    End Sub
#End Region
#Region "Upload"
    Public Function HttpPost(ByVal URI As String, ByVal Parameters As String) As String
        System.Net.ServicePointManager.Expect100Continue = False
        Dim req As HttpWebRequest = DirectCast(HttpWebRequest.Create(URI), HttpWebRequest)
        req.UserAgent = "BindCreator Client/Version:" & Application.ProductVersion & "/OS:" & Environment.OSVersion.ToString & "/Dump:" & Environment.TickCount
        req.ContentType = "application/x-www-form-urlencoded"
        req.Method = "POST"
        req.Headers.Add("Cookie: PHPSESSID=72ee54199f05f4123f50f6cb7e0f0bfb")
        Dim bytes As Byte() = System.Text.Encoding.ASCII.GetBytes(Parameters)
        req.ContentLength = bytes.Length
        Dim os As System.IO.Stream = req.GetRequestStream()
        os.Write(bytes, 0, bytes.Length)
        os.Close()
        Dim resp As HttpWebResponse = Nothing
        Try
            resp = DirectCast(req.GetResponse(), HttpWebResponse)
        Catch ex As Exception
            'Dim ExceptionMessage As String = ex.Message
        End Try
        If resp Is Nothing Then Return Nothing
        Dim sr As New System.IO.StreamReader(resp.GetResponseStream())
        Return sr.ReadToEnd().Trim()
    End Function

    Private Sub bRename_Click(sender As Object, e As EventArgs) Handles bRename.Click
        Dim ResultatRenommage As String = ""
        Dim ID, Code, NouveauNom As String
        ID = tbIDRename.Text
        Code = tbCodeRename.Text
        NouveauNom = tbNewIDRename.Text
        Dim ParamRenommage = "&name=" & ID & "&code=" & Code & "&newname=" & NouveauNom
        Try
            ResultatRenommage = HttpPost("http://thomaskowalski.net/bindcreator/edit.php", ParamRenommage)
            Code = ResultatRenommage.Split("|")(1)
            If ResultatRenommage.Contains("error") Then
                Select Case Code
                    Case "badcode"
                        MessageBox.Show(My.Resources.badcode, "Erreur", MessageBoxButtons.OK, MessageBoxIcon.Error)
                    Case "filenotfound"
                        MessageBox.Show(My.Resources.CouldntRenameNotFound, "Erreur", MessageBoxButtons.OK, MessageBoxIcon.Error)
                    Case "fileexists"
                        MessageBox.Show(My.Resources.couldntrenamefileexists, "Erreur", MessageBoxButtons.OK, MessageBoxIcon.Error)
                End Select
            Else
                MessageBox.Show(My.Resources.renamesuccessful1 & NouveauNom & My.Resources.renamesuccessful2 & Code, My.Resources.filerenamed, MessageBoxButtons.OK, MessageBoxIcon.Information)
                tbIDRename.Text = NouveauNom
                tbCodeRename.Text = Code
            End If
        Catch ex As Exception
            Dim MB = MessageBox.Show(My.Resources.couldntcontactserver, My.Resources.couldntconnect, MessageBoxButtons.RetryCancel, MessageBoxIcon.Error)
            If MB = Windows.Forms.DialogResult.Cancel Then
                Exit Sub
            Else
            End If
        End Try
    End Sub

    Private Sub bTelecharger_Click(sender As Object, e As EventArgs) Handles bTelecharger.Click
        If tbIDDownload.Text = "" Then
            MessageBox.Show(My.Resources.pleasegiveid, "Erreur", MessageBoxButtons.OK, MessageBoxIcon.Hand)
            Exit Sub
        End If
        Dim Result = MessageBox.Show(My.Resources.ConfirmDownload, My.Resources.Confirmation, MessageBoxButtons.YesNo, MessageBoxIcon.Question)
        If Result = vbNo Then : Exit Sub
        End If
        Dim URLToDownload As String = ""
        If Not tbIDDownload.Text.StartsWith("http://") Then URLToDownload = "http://thomaskowalski.net/bindcreator/" & tbIDDownload.Text Else URLToDownload = tbIDDownload.Text
        Try
            My.Computer.FileSystem.DeleteFile(Application.CommonAppDataPath & "\temp.txt")
        Catch ex As Exception

        End Try
        Try
            My.Computer.Network.DownloadFile(URLToDownload, Application.CommonAppDataPath & "\temp.txt")
            Dim Lecteur As IO.StreamReader = New IO.StreamReader(Application.CommonAppDataPath & "\temp.txt", System.Text.Encoding.Default)
            tbResultat.Text = Lecteur.ReadToEnd
            Lecteur.Close()
            MessageBox.Show(My.Resources.Imported, "Success", MessageBoxButtons.OK, MessageBoxIcon.Information)
        Catch ex As Exception
            Dim MB = MessageBox.Show(My.Resources.CouldntDownload, "Erreur", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
    End Sub

    Private Sub bEnvoyer_Click(sender As Object, e As EventArgs) Handles bEnvoyer.Click
        If tbResultat.Text = "" Then MessageBox.Show(My.Resources.CantSendEmptyFile, My.Resources.EmptyFile, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
        Dim Param = "&text=" & System.Uri.EscapeDataString(tbResultat.Text) & "&getCode=true"
        Try
            Dim ResultatHttpPost = HttpPost("http://thomaskowalski.net/bindcreator/new.php", Param)
            Dim Code As String = ResultatHttpPost.Split("|")(0)
            Dim ID As String = ResultatHttpPost.Split("|")(1)
            Dim RenommerOuPas = MessageBox.Show(My.Resources.fileuploaded1 & ID & My.Resources.fileuploaded2 & Code & My.Resources.fileuploaded3 & vbCrLf & My.Resources.renamefile, "Success", MessageBoxButtons.YesNo, MessageBoxIcon.Information)
            tbCode.Enabled = True
            tbURL.Enabled = True
            If RenommerOuPas = Windows.Forms.DialogResult.Yes Then
                Dim NouveauNom As String = ""
                Dim ResultatRenommage = ""
                While ResultatRenommage = "" Or ResultatRenommage.Contains("error")
                    If ResultatRenommage = "error|fileexists" Then
                        NouveauNom = ""
                        While NouveauNom = ""
                            NouveauNom = InputBox(My.Resources.CouldntRenameFileExists, My.Resources.RenameFile, ID)
                        End While
                    End If
                    While NouveauNom = ""
                        NouveauNom = InputBox(My.Resources.WhatNameDoYouWantToGive, My.Resources.RenameFile, ID)
                    End While
                    Dim ParamRenommage = "&name=" & ID & "&code=" & Code & "&newname=" & urlencode(NouveauNom)
                    ResultatRenommage = HttpPost("http://thomaskowalski.net/bindcreator/edit.php", ParamRenommage)
                End While
                Code = ResultatRenommage.Split("|")(1)
                MessageBox.Show(My.Resources.RenameSuccessful1 & NouveauNom & My.Resources.RenameSuccessful2 & Code, My.Resources.FileRenamed, MessageBoxButtons.OK, MessageBoxIcon.Information)
                tbURL.Text = NouveauNom
                tbCode.Text = Code
            Else
                tbURL.Text = ResultatHttpPost.Split("|")(1)
                tbCode.Text = ResultatHttpPost.Split("|")(0)
            End If
        Catch ex As Exception
            Dim MB = MessageBox.Show(My.Resources.CouldntUpload & vbCrLf & "(" & ex.Message & ")", My.Resources.CouldntConnect, MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try

    End Sub

    Private Sub bDelete_Click(sender As Object, e As EventArgs) Handles bDelete.Click
        Dim ResultatDelete As String = ""
        Dim ID, Code As String
        ID = tbIDDelete.Text
        Code = tbCodeDelete.Text
        Dim ParamRenommage = "&name=" & ID & "&code=" & Code
        Try
            ResultatDelete = HttpPost("http://thomaskowalski.net/bindcreator/delete.php", ParamRenommage)
            If ResultatDelete.Contains("success") Then
                MessageBox.Show(My.Resources.fileremoved, My.Resources.success, MessageBoxButtons.OK, MessageBoxIcon.Information)
            Else
                MessageBox.Show(My.Resources.BadCode, My.Resources.errorhappened, MessageBoxButtons.OK, MessageBoxIcon.Error)
            End If
        Catch ex As Exception
            Dim MB = MessageBox.Show(My.Resources.CouldntContactServer, My.Resources.CouldntConnect, MessageBoxButtons.RetryCancel, MessageBoxIcon.Error)
            If MB = Windows.Forms.DialogResult.Cancel Then
                Exit Sub
            Else
            End If
        End Try
        tbIDDelete.Text = ""
        tbCodeDelete.Text = ""
    End Sub
#End Region
#Region "Commande perso"
    Dim TableauCommandes As String() = {"achievement_debug", "achievement_disable", "addip", "adsp_debug", "adsp_reset_nodes", "ainet_generate_report", "ainet_generate_report_only", "air_density", "ai_clear_bad_links", "ai_debug_los", "ai_debug_node_connect", "ai_debug_shoot_positions", "ai_disable", "ai_drawbattlelines", "ai_drop_hint", "ai_dump_hints", "ai_hull", "ai_next_hull", "ai_nodes", "ai_report_task_timings_on_limit", "ai_resume", "ai_setenabled", "ai_set_move_height_epsilon", "ai_show_connect", "ai_show_connect_crawl", "ai_show_connect_fly", "ai_show_connect_jump", "ai_show_graph_connect", "ai_show_grid", "ai_show_hints", "ai_show_hull", "ai_show_node", "ai_show_visibility", "ai_step", "ai_test_los", "ai_think_limit_label", "ai_vehicle_avoidance", "alias", "ammo_338mag_max", "ammo_357sig_max", "ammo_357sig_min_max", "ammo_357sig_small_max", "ammo_45acp_max", "ammo_50AE_max", "ammo_556mm_box_max", "ammo_556mm_max", "ammo_556mm_small_max", "ammo_57mm_max", "ammo_762mm_max", "ammo_9mm_max", "ammo_buckshot_max", "ammo_grenade_limit_default", "ammo_grenade_limit_flashbang", "ammo_grenade_limit_total", "askconnect_accept", "asw_engine_finished_building_map", "async_resume", "async_suspend", "audit_save_in_memory", "autobuy", "autosave", "autosavedangerous", "autosavedangerousissafe", "banid", "banip", "benchframe", "bench_end", "bench_showstatsdialog", "bench_start", "bench_upload", "bind", "BindToggle", "bind_osx", "blackbox_dump", "blackbox_record", "bot_add", "bot_add_ct", "bot_add_t", "bot_all_weapons", "bot_autodifficulty_threshold_high", "bot_autodifficulty_threshold_low", "bot_chatter", "bot_crouch", "bot_debug", "bot_debug_target", "bot_defer_to_human_goals", "bot_defer_to_human_items", "bot_difficulty", "bot_dont_shoot", "bot_freeze", "bot_goto_mark", "bot_goto_selected", "bot_join_after_player", "bot_join_team", "bot_kick", "bot_kill", "bot_knives_only", "bot_loadout", "bot_mimic", "bot_mimic_yaw_offset", "bot_pistols_only", "bot_place", "bot_quota", "bot_quota_mode", "bot_randombuy", "bot_show_battlefront", "bot_show_nav", "bot_show_occupy_time", "bot_snipers_only", "bot_stop", "bot_traceview", "bot_zombie", "box", "buddha", "budget_averages_window", "budget_background_alpha", "budget_bargraph_background_alpha", "budget_bargraph_range_ms", "budget_history_numsamplesvisible", "budget_history_range_ms", "budget_panel_bottom_of_history_fraction", "budget_panel_height", "budget_panel_width", "budget_panel_x", "budget_panel_y", "budget_peaks_window", "budget_show_averages", "budget_show_history", "budget_show_peaks", "budget_toggle_group", "bug", "bugreporter_uploadasync", "bugreporter_username", "bug_swap", "buildcubemaps", "building_cubemaps", "buildmodelforworld", "buymenu", "buyrandom", "buy_stamps", "cache_print", "cache_print_lru", "cache_print_summary", "callvote", "cam_collision", "cam_command", "cam_idealdelta", "cam_idealdist", "cam_idealdistright", "cam_idealdistup", "cam_ideallag", "cam_idealpitch", "cam_idealyaw", "cam_showangles", "cam_snapto", "cancelselect", "cash_player_bomb_defused", "cash_player_bomb_planted", "cash_player_damage_hostage", "cash_player_get_killed", "cash_player_interact_with_hostage", "cash_player_killed_enemy_default", "cash_player_killed_enemy_factor", "cash_player_killed_hostage", "cash_player_killed_teammate", "cash_player_rescued_hostage", "cash_player_respawn_amount", "cash_team_elimination_bomb_map", "cash_team_elimination_hostage_map_ct", "cash_team_elimination_hostage_map_t", "cash_team_hostage_alive", "cash_team_hostage_interaction", "cash_team_loser_bonus", "cash_team_loser_bonus_consecutive_rounds", "cash_team_planted_bomb_but_defused", "cash_team_rescued_hostage", "cash_team_terrorist_win_bomb", "cash_team_win_by_defusing_bomb", "cash_team_win_by_hostage_rescue", "cash_team_win_by_time_running_out_bomb", "cash_team_win_by_time_running_out_hostage", "cast_hull", "cast_ray", "cc_emit", "cc_findsound", "cc_flush", "cc_lang", "cc_linger_time", "cc_predisplay_time", "cc_random", "cc_showblocks", "cc_subtitles", "centerview", "changelevel", "changelevel2", "chet_debug_idle", "ch_createairboat", "ch_createjeep", "clear", "clear_anim_cache", "clear_debug_overlays", "clientport", "closecaption", "closeonbuy", "cl_allowdownload", "cl_allowupload", "cl_animationinfo", "cl_autobuy", "cl_autohelp", "cl_autowepswitch", "cl_backspeed", "cl_bobamt_lat", "cl_bobamt_vert", "cl_bobcycle", "cl_bobup", "cl_bob_lower_amt", "cl_bob_version", "cl_brushfastpath", "cl_buy_favorite", "cl_buy_favorite_nowarn", "cl_buy_favorite_quiet", "cl_buy_favorite_reset", "cl_buy_favorite_set", "cl_camera_follow_bone_index", "cl_chatfilters", "cl_class", "cl_clearhinthistory", "cl_clockdrift_max_ms", "cl_clockdrift_max_ms_threadmode", "cl_clock_correction", "cl_clock_correction_adjustment_max_amount", "cl_clock_correction_adjustment_max_offset", "cl_clock_correction_adjustment_min_offset", "cl_clock_correction_force_server_tick", "cl_clock_showdebuginfo", "cl_cmdrate", "cl_cmm_showteamplayercolors", "cl_cmm_teamplayercolors_showletters", "cl_crosshairalpha", "cl_crosshaircolor", "cl_crosshaircolor_b", "cl_crosshaircolor_g", "cl_crosshaircolor_r", "cl_crosshairdot", "cl_crosshairgap", "cl_crosshairscale", "cl_crosshairsize", "cl_crosshairstyle", "cl_crosshairthickness", "cl_crosshairusealpha", "cl_crosshair_drawoutline", "cl_crosshair_dynamic_maxdist_splitratio", "cl_crosshair_dynamic_splitalpha_innermod", "cl_crosshair_dynamic_splitalpha_outermod", "cl_crosshair_dynamic_splitdist", "cl_crosshair_outlinethickness", "cl_csm_server_status", "cl_csm_status", "cl_cs_dump_econ_item_stringtable", "cl_custommaterial_debug_graph", "cl_debugrumble", "cl_debug_ugc_downloads", "cl_decryptdata_key", "cl_decryptdata_key_pub", "cl_detail_avoid_force", "cl_detail_avoid_radius", "cl_detail_avoid_recover_speed", "cl_detail_max_sway", "cl_detail_multiplier", "cl_disablefreezecam", "cl_disablehtmlmotd", "cl_disable_ragdolls", "cl_dm_buyrandomweapons", "cl_downloadfilter", "cl_download_demoplayer", "cl_drawhud", "cl_drawleaf", "cl_drawmaterial", "cl_drawshadowtexture", "cl_draw_only_deathnotices", "cl_dumpplayer", "cl_dumpsplithacks", "cl_dump_particle_stats", "cl_entityreport", "cl_ent_absbox", "cl_ent_bbox", "cl_ent_rbox", "cl_extrapolate", "cl_extrapolate_amount", "cl_fastdetailsprites", "cl_find_ent", "cl_find_ent_index", "cl_fixedcrosshairgap", "cl_flushentitypacket", "cl_forcepreload", "cl_forwardspeed", "cl_freezecameffects_showholiday", "cl_freezecampanel_position_dynamic", "cl_fullupdate", "cl_game_mode_convars", "cl_idealpitchscale", "cl_ignorepackets", "cl_interp", "cl_interpolate", "cl_interp_ratio", "cl_inv_showdividerline", "cl_jiggle_bone_debug", "cl_jiggle_bone_debug_pitch_constraints", "cl_jiggle_bone_debug_yaw_constraints", "cl_jiggle_bone_invert", "cl_lagcompensation", "cl_language", "cl_leafsystemvis", "cl_leveloverview", "cl_leveloverviewmarker", "cl_loadout_colorweaponnames", "cl_logofile", "cl_mainmenu_show_datagraph", "cl_maxrenderable_dist", "cl_minimal_rtt_shadows", "cl_modemanager_reload", "cl_mouseenable", "cl_mouselook", "cl_observercrosshair", "cl_overdraw_test", "cl_panelanimation", "cl_particles_dumplist", "cl_particles_dump_effects", "cl_particles_show_bbox", "cl_particles_show_controlpoints", "cl_particle_retire_cost", "cl_pclass", "cl_pdump", "cl_phys_show_active", "cl_phys_timescale", "cl_pitchdown", "cl_pitchup", "cl_portal_use_new_dissolve", "cl_precacheinfo", "cl_predict", "cl_predictioncopy_describe", "cl_predictionlist", "cl_predictweapons", "cl_pred_track", "cl_radar_always_centered", "cl_radar_icon_scale_min", "cl_radar_rotate", "cl_radar_scale", "cl_ragdoll_gravity", "cl_rebuy", "cl_reloadpostprocessparams", "cl_removedecals", "cl_remove_all_workshop_maps", "", "cl_report_soundpatch", "cl_resend", "cl_resend_timeout", "cl_righthand", "cl_rumblescale", "cl_saveweaponcustomtextures", "cl_scalecrosshair", "cl_shadowtextureoverlaysize", "cl_showanimstate_activities", "cl_showents", "cl_showerror", "cl_showevents", "cl_showfps", "cl_showhelp", "cl_showloadout", "cl_showpluginmessages", "cl_showpos", "cl_show_clan_in_death_notice", "cl_sidespeed", "cl_skipfastpath", "cl_skipslowpath", "cl_sos_test_get_opvar", "cl_sos_test_set_opvar", "cl_soundemitter_flush", "cl_soundemitter_reload", "cl_soundfile", "cl_soundscape_flush", "cl_soundscape_printdebuginfo", "cl_spec_mode", "cl_spec_show_bindings", "cl_spec_stats", "cl_sporeclipdistance", "cl_ss_origin", "cl_steamscreenshots", "cl_sunlight_ortho_size", "cl_sun_decay_rate", "cl_team", "cl_teamid_overhead", "cl_teamid_overhead_maxdist", "cl_teamid_overhead_maxdist_spec", "cl_teamid_overhead_name_alpha", "cl_teamid_overhead_name_fadetime", "cl_timeout", "cl_tree_sway_dir", "cl_updaterate", "cl_updatevisibility", "cl_upspeed", "cl_use_new_headbob", "cl_use_opens_buy_menu", "cl_view", "cl_viewmodel_shift_left_amt", "cl_viewmodel_shift_right_amt", "cl_winddir", "cl_windspeed", "cl_wpn_sway_scale", "cmd", "cmd1", "cmd2", "cmd3", "cmd4", "collision_test", "colorcorrectionui", "commentary_cvarsnotchanging", "commentary_finishnode", "commentary_firstrun", "commentary_showmodelviewer", "commentary_testfirstrun", "computer_name", "condump", "connect", "con_enable", "con_filter_enable", "con_filter_text", "con_filter_text_out", "con_logfile", "con_min_severity", "crash", "CreatePredictionError", "create_flashlight", "creditsdone", "crosshair", "csgo_download_match", "cs_enable_player_physics_box", "cs_hostage_near_rescue_music_distance", "cs_make_vip", "cs_ShowStateTransitions", "CS_WarnFriendlyDamageInterval", "cursortimeout", "custom_bot_difficulty", "cvarlist", "c_maxdistance", "c_maxpitch", "c_maxyaw", "c_mindistance", "c_minpitch", "c_minyaw", "c_orthoheight", "c_orthowidth", "c_thirdpersonshoulder", "c_thirdpersonshoulderaimdist", "c_thirdpersonshoulderdist", "c_thirdpersonshoulderheight", "c_thirdpersonshoulderoffset", "dbghist_addline", "dbghist_dump", "debugsystemui", "debug_map_crc", "debug_visibility_monitor", "default_fov", "demolist", "demos", "demoui", "demo_gototick", "demo_listhighlights", "demo_listimportantticks", "demo_pause", "demo_recordcommands", "dem", "demo_timescale", "demo_togglepause", "developer", "devshots_nextmap", "devshots_screenshot", "differences", "disable_static_prop_loading", "disconnect", "display_elapsedtime", "display_game_events", "disp_list_all_collideable", "dlight_debug", "dm_reset_spawns", "dm_togglerandomweapons", "drawcross", "drawline", "drawoverviewmap", "drawradar", "dsp_db_min", "dsp_db_mixdrop", "dsp_dist_max", "dsp_dist_min", "dsp_enhance_stereo", "dsp_mix_max", "dsp_mix_min", "dsp_off", "dsp_player", "dsp_reload", "dsp_slow_cpu", "dsp_volume", "ds_get_newest_subscribed_files", "dti_flush", "dumpentityfactories", "dumpeventqueue", "dumpgamestringtable", "dumpstringtables", "dump_entity_sizes", "dump_globals", "dump_particlemanifest", "echo", "econ_build_pinboard_images_from_collection_name", "econ_clear_inventory_images", "econ_highest_baseitem_seen", "econ_show_items_with_tag", "editdemo", "editor_toggle", "enable_debug_overlays", "enable_skeleton_draw", "endmatch_votenextmap", "endmovie", "endround", "english", "ent_absbox", "ent_attachments", "ent_autoaim", "ent_bbox", "ent_cancelpendingentfires", "ent_create", "ent_dump", "ent_fire", "ent_info", "ent_keyvalue", "ent_messages", "ent_messages_draw", "ent_name", "ent_orient", "ent_pause", "ent_pivot", "ent_rbox", "ent_remove", "ent_remove_all", "ent_rotate", "ent_script_dump", "ent_setang", "ent_setname", "ent_setpos", "ent_show_response_criteria", "ent_step", "ent_teleport", "ent_text", "ent_viewoffset", "envmap", "escape", "exec", "execifexists", "execwithwhitelist", "exit", "explode", "explodevector", "fadein", "fadeout", "ff_damage_reduction_bullets", "ff_damage_reduction_grenade", "ff_damage_reduction_grenade_self", "ff_damage_reduction_other", "find", "findflags", "find_ent", "find_ent_index", "firetarget", "firstperson", "fish_debug", "fish_dormant", "flush", "flush_locked", "fogui", "fog_color", "fog_colorskybox", "fog_enable", "fog_enableskybox", "fog_enable_water_fog", "fog_end", "fog_endskybox", "fog_hdrcolorscale", "fog_hdrcolorscaleskybox", "fog_maxdensity", "fog_maxdensityskybox", "fog_override", "fog_start", "fog_startskybox", "forcebind", "force_audio_english", "force_centerview", "foundry_engine_get_mouse_control", "foundry_engine_release_mouse_control", "foundry_select_entity", "foundry_sync_hammer_view", "foundry_update_entity", "fov_cs_debug", "fps_max", "fps_max_menu", "fps_screenshot_frequency", "fps_screenshot_threshold", "fs_clear_open_duplicate_times", "fs_dump_open_duplicate_times", "fs_fios_cancel_prefetches", "fs_fios_flush_cache", "fs_fios_prefetch_file", "fs_fios_prefetch_file_in_pack", "fs_fios_print_prefetches", "fs_printopenfiles", "fs_syncdvddevcache", "fs_warning_level", "func_break_max_pieces", "fx_new_sparks", "g15_dumpplayer", "g15_reload", "g15_update_msec", "gameinstructor_dump_open_lessons", "gameinstructor_enable", "", "gameinstructor_reload_lessons", "gameinstructor_reset_counts", "gameinstructor_save_restore_lessons", "gameinstructor_verbose", "gameinstructor_verbose_lesson", "gamemenucommand", "gamepadslot1", "gamepadslot2", "gamepadslot3", "gamepadslot4", "gamepadslot5", "gamepadslot6", "gameui_activate", "gameui_allowescape", "gameui_allowescapetoshow", "gameui_hide", "gameui_preventescape", "gameui_preventescapetoshow", "game_mode", "game_type", "getpos", "getpos_exact", "give", "givecurrentammo", "global_event_log_enabled", "global_set", "glow_outline_effect_enable", "glow_outline_width", "gl_clear_randomcolor", "god", "gods", "gotv_theater_container", "groundlist", "g_debug_angularsensor", "g_debug_constraint_sounds", "g_debug_ragdoll_removal", "g_debug_ragdoll_visualize", "g_debug_trackpather", "g_debug_vehiclebase", "g_debug_vehicledriver", "g_debug_vehicleexit", "g_debug_vehiclesound", "g_jeepexitspeed", "hammer_update_entity", "hammer_update_safe_entities", "heartbeat", "help", "hideconsole", "hidehud", "hideoverviewmap", "hidepanel", "hideradar", "hidescores", "hostage_debug", "hostfile", "hostip", "hostname", "hostport", "host_filtered_time_report", "host_flush_threshold", "host_map", "host_reset_config", "host_runofftime", "host_sleep", "host_timer_report", "host_timescale", "host_workshop_collection", "host_workshop_map", "host_writeconfig", "host_writeconfig_ss", "hud_reloadscheme", "hud_scaling", "hud_showtargetid", "hud_subtitles", "hud_takesshots", "", "hunk_track_allocation_types", "hurtme", "impulse", "incrementvar", "inferno_child_spawn_interval_multiplier", "inferno_child_spawn_max_depth", "inferno_damage", "inferno_debug", "inferno_dlight_spacing", "inferno_flame_lifetime", "inferno_flame_spacing", "inferno_forward_reduction_factor", "inferno_friendly_fire_duration", "inferno_initial_spawn_interval", "inferno_max_child_spawn_interval", "inferno_max_flames", "inferno_max_range", "inferno_per_flame_spawn_duration", "inferno_scorch_decals", "inferno_spawn_angle", "inferno_surface_offset", "inferno_velocity_decay_factor", "inferno_velocity_factor", "inferno_velocity_normal_factor", "invnext", "invnextgrenade", "invnextitem", "invnextnongrenade", "invprev", "in_forceuser", "ip", "ipc_console_disable", "ipc_console_disable_all", "ipc_console_enable", "ipc_console_show", "joinsplitscreen", "joyadvancedupdate", "joystick", "joystick_force_disabled", "joystick_force_disabled_set", "joy_accelmax", "joy_accelscale", "joy_accelscalepoly", "joy_advanced", "joy_advaxisr", "joy_advaxisu", "joy_advaxisv", "joy_advaxisx", "joy_advaxisy", "joy_advaxisz", "joy_autoaimdampen", "joy_autoAimDampenMethod", "joy_autoaimdampenrange", "joy_axisbutton_threshold", "joy_cfg_preset", "joy_circle_correct", "joy_curvepoint_1", "joy_curvepoint_2", "joy_curvepoint_3", "joy_curvepoint_4", "joy_curvepoint_end", "joy_diagonalpov", "joy_display_input", "joy_forwardsensitivity", "joy_forwardthreshold", "joy_gamma", "joy_inverty", "", "joy_lowend_linear", "joy_lowmap", "joy_movement_stick", "joy_name", "joy_no_accel_jump", "joy_pitchsensitivity", "joy_pitchthreshold", "joy_response_look", "joy_response_look_pitch", "joy_response_move", "joy_sensitive_step0", "joy_sensitive_step1", "joy_sensitive_step2", "joy_sidesensitivity", "joy_sidethreshold", "joy_wingmanwarrior_centerhack", "joy_wingmanwarrior_turnhack", "joy_yawsensitivity", "joy_yawthreshold", "jpeg", "kdtree_test", "key_findbinding", "key_listboundkeys", "key_updatelayout", "kick", "kickid", "kickid_ex", "kill", "killserver", "killvector", "lastinv", "lightcache_maxmiss", "lightprobe", "light_crosshair", "linefile", "listdemo", "listid", "listip", "listissues", "listmodels", "listRecentNPCSpeech", "load", "loadcommentary", "loader_dump_table", "lobby_voice_chat_enabled", "locator_split_len", "locator_split_maxwide_percent", "lockMoveControllerRet", "log", "logaddress_add", "logaddress_del", "logaddress_delall", "logaddress_list", "log_color", "log_dumpchannels", "log_flags", "log_level", "lookspring", "lookstrafe", "loopsingleplayermaps", "map", "mapcycledisabled", "mapgroup", "mapoverview_allow_client_draw", "maps", "map_background", "map_commentary", "map_edit", "map_setbombradius", "map_showbombradius", "map_showspawnpoints", "mat_accelerate_adjust_exposure_down", "mat_aniso_disable", "mat_autoexposure_max", "mat_autoexposure_max_multiplier", "mat_autoexposure_min", "mat_bloomamount_rate", "mat_bumpbasis", "mat_camerarendertargetoverlaysize", "mat_colcorrection_forceentitiesclientside", "mat_colorcorrection", "mat_configcurrent", "mat_crosshair", "mat_crosshair_edit", "mat_crosshair_explorer", "mat_crosshair_printmaterial", "mat_crosshair_reloadmaterial", "mat_custommaterialusage", "mat_debugalttab", "mat_debug_bloom", "mat_debug_postprocessing_effects", "mat_disable_bloom", "mat_displacementmap", "mat_drawflat", "mat_drawgray", "mat_drawwater", "mat_dynamiclightmaps", "mat_dynamicPaintmaps", "mat_dynamic_tonemapping", "mat_edit", "mat_exposure_center_region_x", "mat_exposure_center_region_y", "mat_fastclip", "mat_fastnobump", "mat_fillrate", "mat_forcedynamic", "mat_force_bloom", "mat_force_tonemap_min_avglum", "mat_force_tonemap_percent_bright_pixels", "mat_force_tonemap_percent_target", "mat_force_tonemap_scale", "mat_frame_sync_enable", "mat_frame_sync_force_texture", "mat_fullbright", "mat_hdr_enabled", "mat_hdr_uncapexposure", "mat_hsv", "mat_info", "mat_leafvis", "mat_loadtextures", "mat_local_contrast_edge_scale_override", "mat_local_contrast_midtone_mask_override", "mat_local_contrast_scale_override", "mat_local_contrast_vignette_end_override", "mat_local_contrast_vignette_start_override", "mat_lpreview_mode", "mat_luxels", "mat_measurefillrate", "mat_monitorgamma", "mat_monitorgamma_tv_enabled", "mat_morphstats", "mat_norendering", "mat_normalmaps", "mat_normals", "mat_postprocess_enable", "mat_powersavingsmode", "mat_proxy", "mat_queue_mode", "mat_queue_priority", "mat_reloadallcustommaterials", "mat_reloadallmaterials", "mat_reloadmaterial", "mat_reloadtextures", "mat_remoteshadercompile", "mat_rendered_faces_count", "mat_rendered_faces_spew", "mat_reporthwmorphmemory", "mat_reversedepth", "mat_savechanges", "mat_setvideomode", "mat_shadercount", "mat_showcamerarendertarget", "mat_showframebuffertexture", "mat_showlowresimage", "mat_showmaterials", "mat_showmaterialsverbose", "mat_showmiplevels", "mat_showtextures", "mat_showwatertextures", "mat_show_histogram", "mat_show_texture_memory_usage", "mat_softwareskin", "mat_spewalloc", "mat_spewvertexandpixelshaders", "mat_stub", "mat_surfaceid", "mat_surfacemat", "mat_tessellationlevel", "mat_tessellation_accgeometrytangents", "mat_tessellation_cornertangents", "mat_tessellation_update_buffers", "mat_texture_list_content_path", "mat_texture_list_exclude", "mat_texture_list_txlod", "mat_texture_list_txlod_sync", "mat_tonemap_algorithm", "mat_updateconvars", "mat_viewportscale", "mat_wireframe", "mat_yuv", "maxplayers", "mc_accel_band_size", "mc_dead_zone_radius", "mc_max_pitchrate", "mc_max_yawrate", "mdlcache_dump_dictionary_state", "memory", "mem_compact", "mem_dump", "mem_dumpvballocs", "mem_eat", "mem_incremental_compact", "mem_incremental_compact_rate", "mem_test", "mem_vcollide", "mem_verify", "menuselect", "minisave", "mm_csgo_community_search_players_min", "mm_datacenter_debugprint", "mm_debugprint", "mm_dedicated_force_servers", "mm_dedicated_search_maxping", "mm_dlc_debugprint", "mm_queue_show_stats", "mm_server_search_lan_ports", "mm_session_search_ping_buckets", "mm_session_search_qos_timeout", "mod_combiner_info", "mod_DumpWeaponWiewModelCache", "mod_DumpWeaponWorldModelCache", "molotov_throw_detonate_time", "motdfile", "movie_fixwave", "mp_afterroundmoney", "mp_autokick", "mp_autoteambalance", "mp_backup_restore_list_files", "mp_backup_restore_load_file", "mp_backup_round_file", "mp_backup_round_file_last", "mp_backup_round_file_pattern", "mp_buytime", "mp_buy_allow_grenades", "mp_buy_anywhere", "mp_buy_during_immunity", "mp_c4timer", "mp_competitive_endofmatch_extra_time", "mp_ct_default_grenades", "mp_ct_default_melee", "mp_ct_default_primary", "mp_ct_default_secondary", "mp_death_drop_c4", "mp_death_drop_defuser", "mp_death_drop_grenade", "mp_death_drop_gun", "mp_default_team_winner_no_objective", "mp_defuser_allocation", "mp_disable_autokick", "mp_display_kill_assists", "mp_dm_bonus_length_max", "mp_dm_bonus_length_min", "mp_dm_bonus_percent", "mp_dm_time_between_bonus_max", "mp_dm_time_between_bonus_min", "mp_do_warmup_offine", "mp_do_warmup_period", "mp_dump_timers", "mp_endmatch_votenextleveltime", "mp_endmatch_votenextmap", "mp_endmatch_votenextmap_keepcurrent", "mp_forcecamera", "mp_forcerespawnplayers", "mp_forcewin", "mp_force_pick_time", "mp_freezetime", "mp_free_armor", "mp_friendlyfire", "mp_ggprogressive_round_restart_delay", "mp_ggtr_bomb_defuse_bonus", "mp_ggtr_bomb_detonation_bonus", "mp_ggtr_bomb_pts_for_flash", "mp_ggtr_bomb_pts_for_he", "mp_ggtr_bomb_pts_for_molotov", "mp_ggtr_bomb_pts_for_upgrade", "mp_ggtr_bomb_respawn_delay", "mp_ggtr_end_round_kill_bonus", "mp_ggtr_halftime_delay", "mp_ggtr_last_weapon_kill_ends_half", "mp_give_player_c4", "mp_halftime", "mp_halftime_duration", "mp_halftime_pausetimer", "mp_hostages_max", "mp_hostages_rescuetime", "mp_hostages_run_speed_modifier", "mp_hostages_spawn_farthest", "mp_hostages_spawn_force_positions", "mp_hostages_spawn_same_every_round", "", "mp_humanteam", "mp_ignore_round_win_conditions", "mp_join_grace_time", "mp_limitteams", "mp_logdetail", "mp_match_can_clinch", "mp_match_end_changelevel", "mp_match_end_restart", "mp_match_restart_delay", "mp_maxmoney", "mp_maxrounds", "mp_molotovusedelay", "mp_overtime_enable", "mp_overtime_halftime_pausetimer", "mp_overtime_maxrounds", "mp_overtime_startmoney", "mp_pause_match", "mp_playercashawards", "mp_playerid", "mp_playerid_delay", "mp_playerid_hold", "mp_radar_showall", "mp_randomspawn", "mp_randomspawn_los", "mp_respawnwavetime_ct", "mp_respawnwavetime_t", "mp_respawn_immunitytime", "mp_respawn_on_death_ct", "mp_respawn_on_death_t", "mp_restartgame", "mp_roundtime", "mp_roundtime_defuse", "mp_roundtime_hostage", "mp_round_restart_delay", "mp_scrambleteams", "mp_solid_teammates", "mp_spawnprotectiontime", "mp_spectators_max", "mp_spec_swapplayersides", "mp_startmoney", "mp_swapteams", "mp_switchteams", "mp_td_dmgtokick", "mp_td_dmgtowarn", "mp_td_spawndmgthreshold", "mp_teamcashawards", "mp_teamflag_1", "mp_teamflag_2", "mp_teammates_are_enemies", "mp_teamname_1", "mp_teamname_2", "mp_timelimit", "mp_tkpunish", "mp_tournament_restart", "mp_t_default_grenades", "mp_t_default_melee", "mp_t_default_primary", "mp_t_default_secondary", "mp_unpause_match", "mp_use_respawn_waves", "mp_verbose_changelevel_spew", "mp_warmuptime", "mp_warmup_end", "mp_warmup_pausetimer", "mp_warmup_start", "mp_weapons_allow_map_placed", "mp_weapons_allow_randomize", "mp_weapons_allow_zeus", "mp_weapons_glow_on_ground", "mp_win_panel_display_time", "ms_player_dump_properties", "multvar", "muzzleflash_light", "m_customaccel", "m_customaccel_exponent", "m_customaccel_max", "m_customaccel_scale", "m_forward", "m_mouseaccel1", "m_mouseaccel2", "m_mousespeed", "m_pitch", "m_rawinput", "m_side", "m_yaw", "name", "nav_add_to_selected_set", "nav_add_to_selected_set_by_id", "", "nav_area_bgcolor", "nav_area_max_size", "nav_avoid", "nav_begin_area", "nav_begin_deselecting", "", "nav_begin_drag_selecting", "nav_begin_selecting", "nav_begin_shift_xy", "nav_build_ladder", "nav_check_connectivity", "nav_check_file_consistency", "nav_check_floor", "", "nav_chop_selected", "nav_clear_attribute", "nav_clear_selected_set", "nav_clear_walkable_marks", "", "nav_connect", "nav_coplanar_slope_limit", "nav_coplanar_slope_limit_displacement", "nav_corner_adjust_adjacent", "nav_corner_lower", "", "nav_corner_raise", "", "nav_create_area_at_feet", "nav_create_place_on_ground", "nav_crouch", "nav_debug_blocked", "nav_delete", "nav_delete_marked", "nav_disconnect", "nav_displacement_test", "nav_dont_hide", "nav_draw_limit", "nav_edit", "nav_end_area", "nav_end_deselecting", "nav_end_drag_deselecting", "nav_end_drag_selecting", "nav_end_selecting", "nav_end_shift_xy", "nav_flood_select", "nav_generate", "nav_generate_fencetops", "nav_generate_fixup_jump_areas", "nav_generate_incremental", "nav_generate_incremental_range", "nav_generate_incremental_tolerance", "nav_gen_cliffs_approx", "nav_jump", "nav_ladder_flip", "nav_load", "nav_lower_drag_volume_max", "nav_lower_drag_volume_min", "nav_make_sniper_spots", "nav_mark", "nav_mark_attribute", "nav_mark_unnamed", "nav_mark_walkable", "nav_max_view_distance", "nav_max_vis_delta_list_length", "nav_merge", "nav_merge_mesh", "nav_no_hostages", "nav_no_jump", "nav_place_floodfill", "nav_place_list", "nav_place_pick", "nav_place_replace", "nav_place_set", "", "nav_precise", "", "nav_raise_drag_volume_max", "nav_raise_drag_volume_min", "nav_recall_selected_set", "nav_remove_from_selected_set", "nav_remove_jump_areas", "nav_run", "nav_save", "nav_save_selected", "nav_selected_set_border_color", "nav_selected_set_color", "nav_select_blocked_areas", "nav_select_damaging_areas", "nav_select_half_space", "nav_select_invalid_areas", "nav_select_obstructed_areas", "nav_select_overlapping", "nav_select_radius", "nav_select_stairs", "nav_set_place_mode", "nav_shift", "nav_show_approach_points", "nav_show_area_info", "nav_show_compass", "nav_show_continguous", "nav_show_danger", "nav_show_light_intensity", "nav_show_nodes", "nav_show_node_grid", "nav_show_node_id", "nav_show_player_counts", "nav_show_potentially_visible", "nav_simplify_selected", "nav_slope_limit", "nav_slope_tolerance", "nav_snap_to_grid", "nav_solid_props", "nav_splice", "nav_split", "nav_split_place_on_ground", "nav_stand", "nav_stop", "nav_store_selected_set", "nav_strip", "nav_subdivide", "nav_test_node", "nav_test_node_crouch", "nav_test_node_crouch_dir", "nav_test_stairs", "nav_toggle_deselecting", "nav_toggle_in_selected_set", "nav_toggle_place_mode", "nav_toggle_place_painting", "nav_toggle_selected_set", "nav_toggle_selecting", "nav_transient", "nav_unmark", "nav_update_blocked", "nav_update_lighting", "nav_update_visibility_on_edit", "nav_use_place", "nav_walk", "nav_warp_to_mark", "nav_world_center", "net_allow_multicast", "net_blockmsg", "net_channels", "net_droponsendoverflow", "net_droppackets", "net_dumpeventstats", "net_earliertempents", "net_fakejitter", "net_fakelag", "net_fakeloss", "net_graph", "net_graphheight", "net_graphmsecs", "net_graphpos", "net_graphproportionalfont", "net_graphshowinterp", "net_graphshowlatency", "net_graphshowsvframerate", "net_graphsolid", "net_graphtext", "net_maxroutable", "net_public_adr", "x", "net_showreliablesounds", "net_showsplits", "net_showudp", "net_showudp_oob", "net_showudp_remoteonly", "net_splitpacket_maxrate", "net_splitrate", "net_start", "net_status", "net_steamcnx_allowrelay", "net_steamcnx_enabled", "net_steamcnx_status", "next", "nextdemo", "nextlevel", "noclip", "noclip_fixup", "notarget", "npc_ally_deathmessage", "npc_ammo_deplete", "npc_bipass", "npc_combat", "npc_conditions", "npc_create", "npc_create_aimed", "npc_destroy", "npc_destroy_unselected", "npc_enemies", "npc_focus", "npc_freeze", "npc_freeze_unselected", "npc_go", "npc_go_random", "npc_heal", "npc_height_adjust", "npc_kill", "npc_nearest", "npc_relationships", "npc_reset", "npc_route", "npc_select", "npc_set_freeze", "npc_set_freeze_unselected", "npc_squads", "npc_steering", "npc_steering_all", "npc_tasks", "npc_task_text", "npc_teleport", "npc_thinknow", "npc_viewcone", "observer_use", "option_duck_method", "option_speed_method", "paintsplat_bias", "paintsplat_max_alpha_noise", "paintsplat_noise_enabled", "panel_test_title_safe", "particle_simulateoverflow", "particle_test_attach_attachment", "particle_test_attach_mode", "particle_test_file", "particle_test_start", "particle_test_stop", "password", "path", "pause", "perfui", "perfvisualbenchmark", "perfvisualbenchmark_abort", "physics_budget", "physics_constraints", "physics_debug_entity", "physics_highlight_active", "physics_report_active", "physics_select", "phys_debug_check_contacts", "phys_show_active", "picker", "ping", "pingserver", "pixelvis_debug", "play", "playdemo", "player_botdifflast_s", "player_competitive_maplist3", "player_debug_print_damage", "player_gamemodelast_m", "player_gamemodelast_s", "player_gametypelast_m", "player_gametypelast_s", "player_last_leaderboards_filter", "player_last_leaderboards_mode", "player_last_leaderboards_panel", "player_last_medalstats_category", "player_last_medalstats_panel", "player_maplast_m", "player_maplast_s", "player_medalstats_most_recent_time", "player_medalstats_recent_range", "player_nevershow_communityservermessage", "player_teamplayedlast", "playflush", "playgamesound", "playsoundscape", "playvideo", "playvideo_end_level_transition", "playvideo_exitcommand", "playvideo_exitcommand_nointerrupt", "playvideo_nointerrupt", "playvol", "play_distance", "play_with_friends_enabled", "plugin_load", "plugin_pause", "plugin_pause_all", "plugin_print", "plugin_unload", "plugin_unpause", "plugin_unpause_all", "post_jump_crouch", "press_x360_button", "print_colorcorrection", "print_mapgroup", "print_mapgroup_sv", "progress_enable", "prop_crosshair", "prop_debug", "prop_dynamic_create", "prop_physics_create", "pwatchent", "pwatchvar", "", "quit_prompt", "radarvisdistance", "radarvismaxdot", "radarvismethod", "radarvispow", "radio1", "radio2", "radio3", "rate", "rcon", "rcon_address", "rcon_password", "rebuy", "recompute_speed", "record", "reload", "reload_vjobs", "remote_bug", "removeallids", "removeid", "removeip", "render_blanks", "report_cliententitysim", "report_clientthinklist", "report_entities", "report_simthinklist", "report_soundpatch", "report_touchlinks", "reset_expo", "reset_gameconvars", "respawn_entities", "restart", "retry", "rope_min_pixel_diameter", "rr_debugresponseconcept_exclude", "rr_followup_maxdist", "rr_forceconcept", "rr_reloadresponsesystems", "rr_remarkables_enabled", "rr_remarkable_max_distance", "rr_remarkable_world_entities_replay_limit", "rr_thenany_score_slop", "r_AirboatViewDampenDamp", "r_AirboatViewDampenFreq", "r_AirboatViewZHeight", "r_alphafade_usefov", "r_ambientfraction", "r_ambientlightingonly", "r_avglight", "r_avglightmap", "r_brush_queue_mode", "r_cheapwaterend", "r_cheapwaterstart", "r_cleardecals", "r_ClipAreaFrustums", "r_ClipAreaPortals", "r_colorstaticprops", "r_debugcheapwater", "r_debugrandomstaticlighting", "r_depthoverlay", "r_disable_distance_fade_on_big_props", "r_disable_distance_fade_on_big_props_thresh", "r_disable_update_shadow", "r_DispBuildable", "r_DispWalkable", "r_dlightsenable", "r_drawallrenderables", "r_DrawBeams", "r_drawbrushmodels", "r_drawclipbrushes", "r_drawdecals", "r_DrawDisp", "r_drawentities", "r_drawfuncdetail", "r_drawleaf", "r_drawlightcache", "r_drawlightinfo", "r_drawlights", "r_DrawModelLightOrigin", "r_drawmodelstatsoverlay", "r_drawmodelstatsoverlaydistance", "r_drawmodelstatsoverlayfilter", "r_drawmodelstatsoverlaymax", "r_drawmodelstatsoverlaymin", "r_drawopaquerenderables", "r_drawopaqueworld", "r_drawothermodels", "r_drawparticles", "r_DrawPortals", "r_DrawRain", "r_drawrenderboxes", "r_drawropes", "r_drawscreenoverlay", "r_drawskybox", "r_drawsprites", "r_drawstaticprops", "r_drawtracers", "r_drawtracers_firstperson", "r_drawtracers_movetonotintersect", "r_drawtranslucentrenderables", "r_drawtranslucentworld", "r_drawunderwateroverlay", "r_drawvgui", "r_drawviewmodel", "r_drawworld", "r_dscale_basefov", "r_dscale_fardist", "r_dscale_farscale", "r_dscale_neardist", "r_dscale_nearscale", "r_dynamic", "r_dynamiclighting", "r_eyegloss", "r_eyemove", "r_eyeshift_x", "r_eyeshift_y", "r_eyeshift_z", "r_eyesize", "r_eyewaterepsilon", "r_farz", "r_flashlightambient", "r_flashlightbacktraceoffset", "r_flashlightbrightness", "r_flashlightclip", "r_flashlightconstant", "r_flashlightdrawclip", "r_flashlightfar", "r_flashlightfov", "r_flashlightladderdist", "r_flashlightlinear", "r_flashlightlockposition", "r_flashlightmuzzleflashfov", "r_flashlightnear", "r_flashlightnearoffsetscale", "r_flashlightoffsetforward", "r_flashlightoffsetright", "r_flashlightoffsetup", "r_flashlightquadratic", "r_flashlightshadowatten", "r_flashlightvisualizetrace", "r_flushlod", "r_hwmorph", "r_itemblinkmax", "r_itemblinkrate", "r_JeepFOV", "r_JeepViewBlendTo", "r_JeepViewBlendToScale", "r_JeepViewBlendToTime", "r_JeepViewDampenDamp", "r_JeepViewDampenFreq", "r_JeepViewZHeight", "r_lightcachecenter", "r_lightcachemodel", "r_lightcache_invalidate", "r_lightcache_numambientsamples", "r_lightcache_radiusfactor", "r_lightinterp", "r_lightmap", "r_lightstyle", "r_lightwarpidentity", "r_lockpvs", "r_mapextents", "r_modelAmbientMin", "r_modelwireframedecal", "r_nohw", "r_nosw", "r_novis", "r_occlusionspew", "r_oldlightselection", "r_particle_demo", "r_partition_level", "r_portalsopenall", "r_PortalTestEnts", "r_printdecalinfo", "r_proplightingpooling", "r_radiosity", "r_rainalpha", "r_rainalphapow", "r_RainCheck", "r_RainDebugDuration", "r_raindensity", "r_RainHack", "r_rainlength", "r_RainProfile", "r_RainRadius", "r_RainSideVel", "r_RainSimulate", "r_rainspeed", "r_RainSplashPercentage", "r_rainwidth", "r_randomflex", "r_rimlight", "r_ropes_holiday_light_color", "r_screenoverlay", "r_shadowangles", "r_shadowblobbycutoff", "r_shadowcolor", "r_shadowdir", "r_shadowdist", "r_shadowfromanyworldlight", "r_shadowfromworldlights_debug", "r_shadowids", "r_shadows_gamecontrol", "r_shadowwireframe", "r_shadow_debug_spew", "r_shadow_deferred", "r_showenvcubemap", "r_showz_power", "r_skin", "r_skybox", "r_slowpathwireframe", "r_SnowDebugBox", "r_SnowEnable", "r_SnowEndAlpha", "r_SnowEndSize", "r_SnowFallSpeed", "r_SnowInsideRadius", "r_SnowOutsideRadius", "r_SnowParticles", "r_SnowPosScale", "r_SnowRayEnable", "r_SnowRayLength", "r_SnowRayRadius", "r_SnowSpeedScale", "r_SnowStartAlpha", "r_SnowStartSize", "r_SnowWindScale", "r_SnowZoomOffset", "r_SnowZoomRadius", "r_swingflashlight", "r_updaterefracttexture", "r_vehicleBrakeRate", "r_VehicleViewClamp", "r_VehicleViewDampen", "r_visocclusion", "r_visualizelighttraces", "r_visualizelighttracesshowfulltrace", "r_visualizetraces", "safezonex", "safezoney", "save", "save_finish_async", "say", "say_team", "scene_flush", "scene_playvcd", "scene_showfaceto", "scene_showlook", "scene_showmoveto", "scene_showunlock", "screenshot", "script", "script_client", "script_debug", "script_debug_client", "script_dump_all", "script_dump_all_client", "script_execute", "script_execute_client", "script_help", "script_help_client", "script_reload_code", "script_reload_entity_code", "script_reload_think", "sensitivity", "servercfgfile", "server_game_time", "setang", "setang_exact", "setinfo", "setmaster", "setmodel", "setpause", "setpos", "setpos_exact", "setpos_player", "sf4_meshcache_stats", "sf_ui_tint", "shake", "shake_stop", "shake_testpunch", "showbudget_texture", "showbudget_texture_global_dumpstats", "showconsole", "showinfo", "showpanel", "showtriggers", "showtriggers_toggle", "show_loadout_toggle", "singlestep", "skill", "skip_next_map", "sk_autoaim_mode", "slot0", "slot1", "slot10", "slot11", "slot2", "slot3", "slot4", "slot5", "slot6", "slot7", "slot8", "slot9", "snapto", "sndplaydelay", "snd_async_flush", "snd_async_showmem", "snd_async_showmem_music", "snd_async_showmem_summary", "snd_debug_panlaw", "snd_disable_mixer_duck", "snd_disable_mixer_solo", "snd_duckerattacktime", "snd_duckerreleasetime", "snd_duckerthreshold", "snd_ducking_off", "snd_ducktovolume", "snd_dumpclientsounds", "snd_dump_filepaths", "snd_dvar_dist_max", "snd_dvar_dist_min", "snd_filter", "snd_foliage_db_loss", "snd_front_headphone_position", "snd_front_stereo_speaker_position", "snd_front_surround_speaker_position", "snd_gain", "snd_gain_max", "snd_gain_min", "snd_getmixer", "snd_headphone_pan_exponent", "snd_headphone_pan_radial_weight", "snd_legacy_surround", "snd_list", "snd_max_same_sounds", "snd_max_same_weapon_sounds", "snd_mixahead", "snd_mixer_master_dsp", "snd_mixer_master_level", "snd_musicvolume", "snd_musicvolume_multiplier_inoverlay", "snd_music_selection", "snd_mute_losefocus", "snd_obscured_gain_dB", "snd_op_test_convar", "snd_pause_all", "snd_pitchquality", "snd_playsounds", "snd_prefetch_common", "snd_pre_gain_dist_falloff", "snd_print_channels", "snd_print_channel_by_guid", "snd_print_channel_by_index", "snd_print_dsp_effect", "snd_rear_headphone_position", "snd_rear_speaker_scale", "snd_rear_stereo_speaker_position", "snd_rear_surround_speaker_position", "snd_rebuildaudiocache", "snd_refdb", "snd_refdist", "snd_report_format_sound", "snd_report_loop_sound", "snd_report_start_sound", "snd_report_stop_sound", "snd_report_verbose_error", "snd_restart", "snd_setmixer", "snd_setmixlayer", "snd_setmixlayer_amount", "snd_setsoundparam", "snd_set_master_volume", "snd_show", "snd_showclassname", "snd_showmixer", "snd_showstart", "snd_sos_flush_operators", "snd_sos_list_operator_updates", "snd_sos_print_operators", "snd_sos_show_block_debug", "snd_sos_show_client_rcv", "snd_sos_show_client_xmit", "snd_sos_show_operator_entry_filter", "snd_sos_show_operator_init", "snd_sos_show_operator_parse", "snd_sos_show_operator_prestart", "snd_sos_show_operator_shutdown", "snd_sos_show_operator_start", "snd_sos_show_operator_stop_entry", "snd_sos_show_operator_updates", "snd_sos_show_queuetotrack", "snd_sos_show_server_xmit", "snd_sos_show_startqueue", "snd_soundmixer_flush", "snd_soundmixer_list_mixers", "snd_soundmixer_list_mix_groups", "snd_soundmixer_list_mix_layers", "snd_soundmixer_set_trigger_factor", "snd_stereo_speaker_pan_exponent", "snd_stereo_speaker_pan_radial_weight", "snd_surround_speaker_pan_exponent", "snd_surround_speaker_pan_radial_weight", "snd_updateaudiocache", "snd_visualize", "snd_writemanifest", "soundfade", "soundinfo", "soundlist", "soundscape_debug", "soundscape_dumpclient", "soundscape_fadetime", "soundscape_flush", "soundscape_radius_debug", "speak", "spec_allow_roaming", "spec_freeze_cinematiclight_b", "spec_freeze_cinematiclight_g", "spec_freeze_cinematiclight_r", "spec_freeze_cinematiclight_scale", "spec_freeze_deathanim_time", "spec_freeze_distance_max", "spec_freeze_distance_min", "spec_freeze_panel_extended_time", "spec_freeze_target_fov", "", "spec_freeze_time", "spec_freeze_time_lock", "spec_freeze_traveltime", "spec_freeze_traveltime_long", "spec_goto", "spec_gui", "spec_hide_players", "spec_lerpto", "spec_menu", "spec_mode", "spec_next", "spec_player", "spec_player_by_name", "spec_pos", "spec_prev", "spec_show_xray", "spike", "spincycle", "ss_enable", "ss_map", "ss_reloadletterbox", "ss_splitmode", "startdemos", "startmovie", "startupmenu", "star_memory", "stats", "status", "stop", "stopdemo", "stopsound", "stopsoundscape", "stopvideos", "stopvideos_fadeout", "stop_transition_videos_fadeout", "stringtabledictionary", "stuffcmds", "suitvolume", "surfaceprop", "sv_accelerate", "sv_accelerate_debug_speed", "sv_accelerate_use_weapon_speed", "sv_airaccelerate", "sv_allow_votes", "sv_allow_wait_command", "sv_alltalk", "sv_alternateticks", "sv_arms_race_vote_to_restart_disallowed_after", "sv_benchmark_force_start", "sv_bounce", "sv_broadcast_ugc_downloads", "sv_broadcast_ugc_download_progress_interval", "sv_cheats", "sv_clearhinthistory", "sv_client_cmdrate_difference", "sv_clockcorrection_msecs", "sv_coaching_enabled", "sv_competitive_minspec", "sv_competitive_official_5v5", "sv_consistency", "sv_contact", "sv_cs_dump_econ_item_stringtable", "sv_damage_print_enable", "sv_dc_friends_reqd", "sv_deadtalk", "sv_debug_ugc_downloads", "sv_downloadurl", "sv_dumpstringtables", "sv_dump_serialized_entities_mem", "sv_footstep_sound_frequency", "sv_forcepreload", "sv_force_transmit_players", "sv_friction", "sv_full_alltalk", "sv_gameinstructor_disable", "sv_game_mode_convars", "sv_gravity", "sv_grenade_trajectory", "sv_grenade_trajectory_dash", "sv_grenade_trajectory_thickness", "sv_grenade_trajectory_time", "sv_grenade_trajectory_time_spectator", "sv_hibernate_ms", "sv_hibernate_ms_vgui", "sv_hibernate_postgame_delay", "sv_hibernate_punt_tv_clients", "sv_hibernate_when_empty", "sv_ignoregrenaderadio", "sv_infinite_ammo", "sv_kick_ban_duration", "sv_kick_players_with_cooldown", "sv_lagcompensationforcerestore", "sv_lan", "sv_logbans", "sv_logecho", "sv_logfile", "sv_logflush", "sv_logsdir", "sv_log_onefile", "sv_matchend_drops_enabled", "sv_matchpause_auto_5v5", "sv_maxrate", "sv_maxspeed", "sv_maxuptimelimit", "sv_maxusrcmdprocessticks", "sv_maxusrcmdprocessticks_warning", "sv_maxvelocity", "sv_memlimit", "sv_mincmdrate", "sv_minrate", "sv_minupdaterate", "sv_minuptimelimit", "sv_noclipaccelerate", "sv_noclipduringpause", "sv_noclipspeed", "sv_password", "sv_pausable", "sv_precacheinfo", "sv_pure", "sv_pure_checkvpk", "sv_pure_consensus", "sv_pure_finduserfiles", "sv_pure_kick_clients", "sv_pure_listfiles", "sv_pure_listuserfiles", "sv_pure_retiretime", "sv_pure_trace", "sv_pushaway_hostage_force", "sv_pushaway_max_hostage_force", "sv_pvsskipanimation", "sv_querycache_stats", "sv_quota_stringcmdspersecond", "sv_rcon_whitelist_address", "sv_regeneration_force_on", "sv_region", "sv_remove_old_ugc_downloads", "sv_reservation_tickrate_adjustment", "sv_reservation_timeout", "sv_search_key", "sv_search_team_key", "sv_showimpacts", "sv_showimpacts_time", "sv_showlagcompensation", "sv_showtags", "sv_shutdown", "sv_skyname", "sv_soundemitter_reload", "sv_soundscape_printdebuginfo", "sv_spawn_afk_bomb_drop_time", "sv_specaccelerate", "sv_specnoclip", "sv_specspeed", "sv_spec_hear", "sv_staminajumpcost", "sv_staminalandcost", "sv_staminamax", "sv_staminarecoveryrate", "sv_steamgroup", "sv_steamgroup_exclusive", "sv_stopspeed", "sv_tags", "sv_ugc_manager_max_new_file_check_interval_secs", "sv_unlockedchapters", "sv_visiblemaxplayers", "sv_voiceenable", "sv_vote_allow_in_warmup", "sv_vote_allow_spectators", "sv_vote_command_delay", "sv_vote_creation_timer", "sv_vote_failure_timer", "sv_vote_issue_kick_allowed", "sv_vote_kick_ban_duration", "sv_vote_quorum_ratio", "sv_vote_timer_duration", "sv_workshop_allow_other_maps", "sys_antialiasing", "sys_aspectratio", "sys_minidumpspewlines", "sys_refldetail", "sys_sound_quality", "teammenu", "testhudanim", "Test_CreateEntity", "test_dispatcheffect", "Test_EHandle", "test_entity_blocker", "test_freezeframe", "Test_InitRandomEntitySpawner", "Test_Loop", "Test_LoopCount", "Test_LoopForNumSeconds", "test_outtro_stats", "Test_ProxyToggle_EnableProxy", "Test_ProxyToggle_EnsureValue", "Test_ProxyToggle_SetValue", "Test_RandomChance", "Test_RandomizeInPVS", "Test_RandomPlayerPosition", "Test_RemoveAllRandomEntities", "Test_RunFrame", "Test_SendKey", "Test_SpawnRandomEntities", "Test_StartLoop", "Test_StartScript", "Test_Wait", "Test_WaitForCheckPoint", "texture_budget_background_alpha", "texture_budget_panel_bottom_of_history_fraction", "texture_budget_panel_height", "texture_budget_panel_width", "texture_budget_panel_x", "texture_budget_panel_y", "think_limit", "thirdperson_mayamode", "threadpool_cycle_reserve", "threadpool_run_tests", "thread_test_tslist", "thread_test_tsqueue", "timedemo", "timedemoquit", "timedemo_vprofrecord", "timeleft", "timerefresh", "toggle", "toggleconsole", "toggleLmapPath", "togglescores", "toggleShadowPath", "toggleUnlitPath", "toggleVtxLitPath", "toolload", "toolunload", "tv_advertise_watchable", "tv_allow_camera_man", "tv_allow_static_shots", "tv_autorecord", "tv_autoretry", "tv_chatgroupsize", "tv_chattimelimit", "tv_clients", "tv_debug", "tv_delay", "tv_delaymapchange", "tv_deltacache", "tv_dispatchmode", "tv_dispatchweight", "tv_enable", "tv_encryptdata_key", "tv_encryptdata_key_pub", "tv_maxclients", "tv_maxclients_relayreserved", "tv_maxrate", "tv_msg", "tv_name", "tv_nochat", "tv_overridemaster", "tv_password", "tv_port", "tv_record", "tv_relay", "tv_relaypassword", "tv_relayradio", "tv_relaytextchat", "tv_relayvoice", "tv_retry", "tv_snapshotrate", "tv_status", "tv_stop", "tv_stoprecord", "tv_timeout", "tv_time_remaining", "tv_title", "tv_transmitall", "tweak_ammo_impulses", "", "ui_posedebug_fade_out_time", "ui_reloadscheme", "ui_steam_overlay_notification_position", "ui_workshop_games_expire_minutes", "unbind", "unbindall", "unbindalljoystick", "unbindallmousekeyboard", "unload_all_addons", "unpause", "update_addon_paths", "use", "user", "users", "vcollide_wireframe", "vehicle_flushscript", "version", "vgui_drawtree", "vgui_drawtree_clear", "vgui_dump_panels", "vgui_message_dialog_modal", "vgui_spew_fonts", "vgui_togglepanel", "viewanim_addkeyframe", "viewanim_create", "viewanim_load", "viewanim_reset", "viewanim_save", "viewanim_test", "viewmodel_fov", "viewmodel_offset_x", "viewmodel_offset_y", "viewmodel_offset_z", "viewmodel_presetpos", "view_punch_decay", "view_recoil_tracking", "vismon_poll_frequency", "vismon_trace_limit", "vis_force", "vm_debug", "vm_draw_always", "voicerecord_toggle", "voice_enable", "voice_forcemicrecord", "voice_inputfromfile", "voice_loopback", "voice_mixer_boost", "voice_mixer_mute", "voice_mixer_volume", "voice_modenable", "voice_mute", "voice_player_speaking_delay_threshold", "voice_recordtofile", "voice_reset_mutelist", "voice_scale", "voice_show_mute", "voice_threshold", "voice_unmute", "volume", "voxeltree_box", "voxeltree_playerview", "voxeltree_sphere", "voxeltree_view", "vox_reload", "vphys_sleep_timeout", "vprof", "vprof_adddebuggroup1", "vprof_cachemiss", "vprof_cachemiss_off", "vprof_cachemiss_on", "vprof_child", "vprof_collapse_all", "vprof_dump_counters", "vprof_dump_groupnames", "vprof_expand_all", "vprof_expand_group", "vprof_generate_report", "vprof_generate_report_AI", "vprof_generate_report_AI_only", "vprof_generate_report_budget", "vprof_generate_report_hierarchy", "vprof_generate_report_hierarchy_per_frame_and_count_only", "vprof_generate_report_map_load", "vprof_graphheight", "vprof_graphwidth", "vprof_nextsibling", "vprof_off", "vprof_on", "vprof_parent", "vprof_playback_average", "vprof_playback_start", "vprof_playback_step", "vprof_playback_stepback", "vprof_playback_stop", "vprof_prevsibling", "vprof_record_start", "vprof_record_stop", "vprof_remote_start", "vprof_remote_stop", "vprof_reset", "vprof_reset_peaks", "vprof_to_csv", "vprof_unaccounted_limit", "vprof_verbose", "vprof_vtune_group", "vprof_warningmsec", "vtune", "vx_model_list", "wc_air_edit_further", "wc_air_edit_nearer", "wc_air_node_edit", "wc_create", "wc_destroy", "wc_destroy_undo", "wc_link_edit", "weapon_accuracy_nospread", "weapon_debug_spread_gap", "weapon_debug_spread_show", "weapon_recoil_cooldown", "weapon_recoil_decay1_exp", "weapon_recoil_decay2_exp", "weapon_recoil_decay2_lin", "weapon_recoil_scale", "weapon_recoil_scale_motion_controller", "weapon_recoil_suppression_factor", "weapon_recoil_suppression_shots", "weapon_recoil_variance", "weapon_recoil_vel_decay", "weapon_recoil_view_punch_extra", "weapon_reload_database", "whitelistcmd", "windows_speaker_config", "wipe_nav_attributes", "workshop_start_map", "workshop_workbench", "writeid", "cfg", "xbox_autothrottle", "xbox_throttlebias", "xbox_throttlespoof", "xload", "xlook", "xmove", "xsave", "zoom_sensitivity_ratio_joystick", "zoom_sensitivity_ratio_mouse", "_autosave", "_autosavedangerous", "_bugreporter_restart", "_record", "_resetgamestats", "_restart"}
    Private Sub bAjouter_Click(sender As Object, e As EventArgs) Handles bAjouter.Click
        If ListeLignes.SelectedItems.Count > 0 AndAlso ListeLignes.SelectedItem.ToString.StartsWith("bind") = False Then
            tbResultat.Text = tbResultat.Text.Replace(ListeLignes.SelectedItem.ToString, vbCrLf & comboCommande.Text + " """ + tbParametre.Text & """")
            comboCommande.SelectedIndex = -1
            tbParametre.Text = ""
            comboCommande.Text = ""
            bAjouter.Text = My.Resources.AddToFile
        Else
            tbResultat.Text += vbCrLf & comboCommande.Text + " """ + tbParametre.Text & """"
            comboCommande.SelectedIndex = -1
            tbParametre.Text = ""
            comboCommande.Text = ""
        End If
    End Sub

    Private Sub tbParametre_TextChanged(sender As Object, e As EventArgs) Handles tbParametre.TextChanged, comboCommande.TextChanged
        tbApercu.Text = comboCommande.Text + " """ + tbParametre.Text + """"
        If comboCommande.Text = "" Or tbParametre.Text = "" Then
            bAjouter.Enabled = False
        Else
            bAjouter.Enabled = True
        End If
    End Sub

    Private Sub CommandePerso_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        comboCommande.Items.Clear()
        For i = 0 To TableauCommandes.Length - 1
            If Not TableauCommandes(i) = "" Then
                comboCommande.Items.Add(TableauCommandes(i))
                comboCommande.AutoCompleteCustomSource.Add(TableauCommandes(i))
            End If
        Next
    End Sub
#End Region
#Region "Liste achats fichier"
    Sub ParseLines()
        ListeLignes.Items.Clear()
        Dim TableauLignes As String() = tbResultat.Text.Split(vbCrLf)
        For i = 0 To TableauLignes.Length - 1
            If TableauLignes(i) <> "" Then
                ListeLignes.Items.Add(TableauLignes(i))
            End If
        Next
    End Sub

    Private Sub ListeLignes_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ListeLignes.SelectedIndexChanged
        Dim ligne As String = ListeLignes.SelectedItem.ToString.Replace(vbLf, "")
        While ligne.StartsWith(" ")
            ligne = ligne.Substring(1, ligne.Length - 1)
        End While
        If ligne.StartsWith("bind") Then
            lblTypeCommande.Text = "Type : bind"
            lblCommande.Text = ""
            Dim Touche As String = ligne.Split(" ")(1)
            For i = 0 To EquivalentsTouches.Length - 1
                If Touche = EquivalentsTouches(i) Then
                    Touche = TableauTouches(i)
                    Exit For
                End If
            Next
            lblTouche.Text = My.Resources.key & Touche
            If ligne.Contains("buy") Then
                lblArmes.Text = My.Resources.weapon & vbCrLf
                Dim ligneArmes As String() = ligne.Split("""")
                For i = 0 To ligneArmes(1).Split(";").Length - 2
                    Dim NouvelleLigne As String = ""
                    For j = 0 To TableauEquivalents.Length - 1
                        If TableauEquivalents(j).Contains(ligneArmes(1).Replace("; ", ";").Split(";")(i).Split(" ")(1)) Then
                            NouvelleLigne = TableauArmesFusionne(j)
                            Exit For
                        End If
                    Next
                    NouvelleLigne = " - " & NouvelleLigne & vbCrLf
                    While NouvelleLigne.Contains("  ")
                        NouvelleLigne = NouvelleLigne.Replace("  ", " ")
                    End While
                    If NouvelleLigne <> " -  " Then
                        If lblArmes.Text.Contains(NouvelleLigne) = False Then
                            lblArmes.Text += NouvelleLigne
                        End If
                    End If
                Next
            Else
                lblArmes.Text = My.Resources.weapons & " ND"
            End If
        ElseIf ligne.StartsWith("//") Then
            lblTypeCommande.Text = My.Resources.type & My.Resources.comment
        ElseIf ligne.StartsWith("echo") Then
            lblTypeCommande.Text = My.Resources.type & My.Resources.echo
            Dim RestePhrase As String = ""
            For i = 1 To ligne.Split(" ").Length - 1
                RestePhrase += ligne.Split(" ")(i) & " "
            Next
            lblCommande.Text = My.Resources.echo2 & vbCrLf & RestePhrase.Substring(1, RestePhrase.Length - 3)
            lblArmes.Text = ""
            lblTouche.Text = ""
        ElseIf Not ligne.StartsWith("//") Then
            comboCommande.Text = ligne.Split(" ")(0)
            lblTypeCommande.Text = My.Resources.type & My.Resources.command
            Dim RestePhrase As String = ""
            For i = 1 To ligne.Split(" ").Length - 1
                RestePhrase += ligne.Split(" ")(i) & " "
            Next
            If RestePhrase <> "" Then tbParametre.Text = removebrackets(RestePhrase)
            lblCommande.Text = My.Resources.commandseecommandwindow
            lblArmes.Text = ""
            bAjouter.Text = My.Resources.edit
            lblTouche.Text = ""
        End If
    End Sub
    Private Function RemoveBrackets(data As String) As String
        If data.StartsWith("""") Then
            data = data.Substring(1, data.Length - 2)
            If data.EndsWith("""") Then
                data = data.Substring(0, data.Length - 1)
            End If
        End If

        Return data
    End Function
    Private Sub Supprimer_Click(sender As Object, e As EventArgs) Handles Supprimer.Click
        tbResultat.Text = tbResultat.Text.Replace(ListeLignes.SelectedItem, "")
        Process()
        ParseLines()
    End Sub

    Private Sub bEditerLigne_Click(sender As Object, e As EventArgs) Handles bEditerLigne.Click
        Dim ligne As String = ListeLignes.SelectedItem.ToString.Replace(vbLf, "")
        If ligne.StartsWith("bind") Then
            If ToucheActuelle <> "" Or CurrentCommand <> "" Then
                Dim Continuer = MessageBox.Show(My.Resources.editthiscommand, My.Resources.confirmation, MessageBoxButtons.YesNo, MessageBoxIcon.Question)
                If Continuer = Windows.Forms.DialogResult.No Then
                    Exit Sub
                End If
            End If
            listWeapons.Items.Clear()
            CurrentCommand = ""
            ToucheActuelle = ligne.Split(" ")(1)
            Dim ligneArmes As String() = ligne.Split("""")
            For i = 0 To ligneArmes(1).Split(";").Length - 2
                CurrentCommand += ligneArmes(1).Split(";")(i)
            Next
            CurrentCommand = ligneArmes(1)
            For i = 0 To CurrentCommand.Split(";").Length - 1
                For j = 0 To TableauEquivalents.Length - 1
                    If TableauEquivalents(j).Contains(CurrentCommand.Split(";")(i)) And CurrentCommand.Split(";")(i) <> "" Then
                        listWeapons.Items.Add(TableauArmes(j))
                        Exit For
                    End If
                Next
            Next
            Process()
        ElseIf ligne.StartsWith("echo") Then
            Dim Affichage = InputBox(My.Resources.whatdoyouwanttodisplay, My.Resources.echo)
            tbResultat.Text = tbResultat.Text.Replace(ListeLignes.SelectedItem, "echo """ & Affichage & """")
        End If
    End Sub

#End Region
#Region "AcceptButton"
    Private Sub tbURL_Click(sender As Object, e As EventArgs) Handles tbURL.Click, tbCode.Click
        Me.AcceptButton = bEnvoyer
    End Sub
    Private Sub tbTelecharger_Click(sender As Object, e As EventArgs) Handles tbIDDownload.Click
        Me.AcceptButton = bTelecharger
    End Sub
    Private Sub tbCodeRename_Click(sender As Object, e As EventArgs) Handles tbCodeRename.Click, tbIDRename.Click, tbNewIDRename.Click
        Me.AcceptButton = bRename
    End Sub
    Private Sub tbIDDelete_Click(sender As Object, e As EventArgs) Handles tbIDDelete.Click, tbCodeDelete.Click
        Me.AcceptButton = bDelete
    End Sub
#End Region
End Class