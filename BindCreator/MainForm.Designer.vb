﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class MainForm
    Inherits System.Windows.Forms.Form

    'Form remplace la méthode Dispose pour nettoyer la liste des composants.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requise par le Concepteur Windows Form
    Private components As System.ComponentModel.IContainer

    'REMARQUE : la procédure suivante est requise par le Concepteur Windows Form
    'Elle peut être modifiée à l'aide du Concepteur Windows Form.  
    'Ne la modifiez pas à l'aide de l'éditeur de code.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(MainForm))
        Me.gbArmes = New System.Windows.Forms.GroupBox()
        Me.lnkAide = New System.Windows.Forms.LinkLabel()
        Me.bOptions = New System.Windows.Forms.Button()
        Me.cEquivalents = New System.Windows.Forms.CheckBox()
        Me.bSwapTeam = New System.Windows.Forms.Button()
        Me.lblNombreAchats = New System.Windows.Forms.Label()
        Me.lblSupprimer = New System.Windows.Forms.LinkLabel()
        Me.bValider = New System.Windows.Forms.Button()
        Me.lblStatus = New System.Windows.Forms.Label()
        Me.lblTouche = New System.Windows.Forms.Label()
        Me.lblWeaponsToBuy = New System.Windows.Forms.Label()
        Me.listWeapons = New System.Windows.Forms.ListBox()
        Me.lblPrix = New System.Windows.Forms.Label()
        Me.lblNom = New System.Windows.Forms.Label()
        Me.picturePreview = New System.Windows.Forms.PictureBox()
        Me.bBack = New System.Windows.Forms.Button()
        Me.b6 = New System.Windows.Forms.Button()
        Me.b5 = New System.Windows.Forms.Button()
        Me.b4 = New System.Windows.Forms.Button()
        Me.b3 = New System.Windows.Forms.Button()
        Me.b2 = New System.Windows.Forms.Button()
        Me.b1 = New System.Windows.Forms.Button()
        Me.gbResultat = New System.Windows.Forms.GroupBox()
        Me.tbExporter = New System.Windows.Forms.Button()
        Me.tbImporter = New System.Windows.Forms.Button()
        Me.bCharger = New System.Windows.Forms.Button()
        Me.bSaveFile = New System.Windows.Forms.Button()
        Me.tbResultat = New System.Windows.Forms.TextBox()
        Me.gbKeyboard = New System.Windows.Forms.GroupBox()
        Me.bKP_Minus = New System.Windows.Forms.Button()
        Me.bKP_Multiply = New System.Windows.Forms.Button()
        Me.bKP_Slash = New System.Windows.Forms.Button()
        Me.bVerrNum = New System.Windows.Forms.Button()
        Me.bKP_PgUp = New System.Windows.Forms.Button()
        Me.bKP_Up = New System.Windows.Forms.Button()
        Me.bKP_Home = New System.Windows.Forms.Button()
        Me.bKP_Plus = New System.Windows.Forms.Button()
        Me.bKP_Right = New System.Windows.Forms.Button()
        Me.bKP_5 = New System.Windows.Forms.Button()
        Me.bKP_Left = New System.Windows.Forms.Button()
        Me.bKP_End = New System.Windows.Forms.Button()
        Me.bKP_Down = New System.Windows.Forms.Button()
        Me.bKP_PgDown = New System.Windows.Forms.Button()
        Me.bKP_Enter = New System.Windows.Forms.Button()
        Me.bKP_Del = New System.Windows.Forms.Button()
        Me.bKP_Ins = New System.Windows.Forms.Button()
        Me.bRightArrow = New System.Windows.Forms.Button()
        Me.bDownArrow = New System.Windows.Forms.Button()
        Me.bLeftArrow = New System.Windows.Forms.Button()
        Me.bUpArrow = New System.Windows.Forms.Button()
        Me.bPgDown = New System.Windows.Forms.Button()
        Me.bEnd = New System.Windows.Forms.Button()
        Me.bDel = New System.Windows.Forms.Button()
        Me.bPgUp = New System.Windows.Forms.Button()
        Me.bHome = New System.Windows.Forms.Button()
        Me.bIns = New System.Windows.Forms.Button()
        Me.gbLignes = New System.Windows.Forms.GroupBox()
        Me.lblArmes = New System.Windows.Forms.Label()
        Me.lblToucheGestionnaireLignes = New System.Windows.Forms.Label()
        Me.lblCommande = New System.Windows.Forms.Label()
        Me.lblTypeCommande = New System.Windows.Forms.Label()
        Me.Supprimer = New System.Windows.Forms.Button()
        Me.bEditerLigne = New System.Windows.Forms.Button()
        Me.ListeLignes = New System.Windows.Forms.ListBox()
        Me.gbCustomCommand = New System.Windows.Forms.GroupBox()
        Me.lblApercuCustom = New System.Windows.Forms.Label()
        Me.tbApercu = New System.Windows.Forms.TextBox()
        Me.tbParametre = New System.Windows.Forms.TextBox()
        Me.lblParametreCustom = New System.Windows.Forms.Label()
        Me.lblCommandeCustom = New System.Windows.Forms.Label()
        Me.comboCommande = New System.Windows.Forms.ComboBox()
        Me.bAjouter = New System.Windows.Forms.Button()
        Me.mainTabView = New System.Windows.Forms.TabControl()
        Me.tabUpload = New System.Windows.Forms.TabPage()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.tbCode = New System.Windows.Forms.TextBox()
        Me.bEnvoyer = New System.Windows.Forms.Button()
        Me.lblURL = New System.Windows.Forms.Label()
        Me.tbURL = New System.Windows.Forms.TextBox()
        Me.tabDownload = New System.Windows.Forms.TabPage()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.tbIDDownload = New System.Windows.Forms.TextBox()
        Me.bTelecharger = New System.Windows.Forms.Button()
        Me.tabRename = New System.Windows.Forms.TabPage()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.tbCodeRename = New System.Windows.Forms.TextBox()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.tbNewIDRename = New System.Windows.Forms.TextBox()
        Me.bRename = New System.Windows.Forms.Button()
        Me.Label9 = New System.Windows.Forms.Label()
        Me.tbIDRename = New System.Windows.Forms.TextBox()
        Me.tabDelete = New System.Windows.Forms.TabPage()
        Me.Label10 = New System.Windows.Forms.Label()
        Me.tbCodeDelete = New System.Windows.Forms.TextBox()
        Me.bDelete = New System.Windows.Forms.Button()
        Me.Label11 = New System.Windows.Forms.Label()
        Me.tbIDDelete = New System.Windows.Forms.TextBox()
        Me.dialogSave = New System.Windows.Forms.SaveFileDialog()
        Me.dialogOuvrir = New System.Windows.Forms.OpenFileDialog()
        Me.workerUpdates = New System.ComponentModel.BackgroundWorker()
        Me.gbArmes.SuspendLayout()
        CType(Me.picturePreview, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.gbResultat.SuspendLayout()
        Me.gbKeyboard.SuspendLayout()
        Me.gbLignes.SuspendLayout()
        Me.gbCustomCommand.SuspendLayout()
        Me.mainTabView.SuspendLayout()
        Me.tabUpload.SuspendLayout()
        Me.tabDownload.SuspendLayout()
        Me.tabRename.SuspendLayout()
        Me.tabDelete.SuspendLayout()
        Me.SuspendLayout()
        '
        'gbArmes
        '
        Me.gbArmes.Controls.Add(Me.lnkAide)
        Me.gbArmes.Controls.Add(Me.bOptions)
        Me.gbArmes.Controls.Add(Me.cEquivalents)
        Me.gbArmes.Controls.Add(Me.bSwapTeam)
        Me.gbArmes.Controls.Add(Me.lblNombreAchats)
        Me.gbArmes.Controls.Add(Me.lblSupprimer)
        Me.gbArmes.Controls.Add(Me.bValider)
        Me.gbArmes.Controls.Add(Me.lblStatus)
        Me.gbArmes.Controls.Add(Me.lblTouche)
        Me.gbArmes.Controls.Add(Me.lblWeaponsToBuy)
        Me.gbArmes.Controls.Add(Me.listWeapons)
        Me.gbArmes.Controls.Add(Me.lblPrix)
        Me.gbArmes.Controls.Add(Me.lblNom)
        Me.gbArmes.Controls.Add(Me.picturePreview)
        Me.gbArmes.Controls.Add(Me.bBack)
        Me.gbArmes.Controls.Add(Me.b6)
        Me.gbArmes.Controls.Add(Me.b5)
        Me.gbArmes.Controls.Add(Me.b4)
        Me.gbArmes.Controls.Add(Me.b3)
        Me.gbArmes.Controls.Add(Me.b2)
        Me.gbArmes.Controls.Add(Me.b1)
        resources.ApplyResources(Me.gbArmes, "gbArmes")
        Me.gbArmes.Name = "gbArmes"
        Me.gbArmes.TabStop = False
        '
        'lnkAide
        '
        resources.ApplyResources(Me.lnkAide, "lnkAide")
        Me.lnkAide.Name = "lnkAide"
        Me.lnkAide.TabStop = True
        '
        'bOptions
        '
        resources.ApplyResources(Me.bOptions, "bOptions")
        Me.bOptions.Name = "bOptions"
        Me.bOptions.UseVisualStyleBackColor = True
        '
        'cEquivalents
        '
        resources.ApplyResources(Me.cEquivalents, "cEquivalents")
        Me.cEquivalents.Checked = True
        Me.cEquivalents.CheckState = System.Windows.Forms.CheckState.Checked
        Me.cEquivalents.Name = "cEquivalents"
        Me.cEquivalents.UseVisualStyleBackColor = True
        '
        'bSwapTeam
        '
        resources.ApplyResources(Me.bSwapTeam, "bSwapTeam")
        Me.bSwapTeam.Name = "bSwapTeam"
        Me.bSwapTeam.UseVisualStyleBackColor = True
        '
        'lblNombreAchats
        '
        resources.ApplyResources(Me.lblNombreAchats, "lblNombreAchats")
        Me.lblNombreAchats.Name = "lblNombreAchats"
        '
        'lblSupprimer
        '
        resources.ApplyResources(Me.lblSupprimer, "lblSupprimer")
        Me.lblSupprimer.Name = "lblSupprimer"
        Me.lblSupprimer.TabStop = True
        '
        'bValider
        '
        resources.ApplyResources(Me.bValider, "bValider")
        Me.bValider.Name = "bValider"
        Me.bValider.UseVisualStyleBackColor = True
        '
        'lblStatus
        '
        resources.ApplyResources(Me.lblStatus, "lblStatus")
        Me.lblStatus.Name = "lblStatus"
        '
        'lblTouche
        '
        resources.ApplyResources(Me.lblTouche, "lblTouche")
        Me.lblTouche.Name = "lblTouche"
        '
        'lblWeaponsToBuy
        '
        resources.ApplyResources(Me.lblWeaponsToBuy, "lblWeaponsToBuy")
        Me.lblWeaponsToBuy.Name = "lblWeaponsToBuy"
        '
        'listWeapons
        '
        Me.listWeapons.FormattingEnabled = True
        resources.ApplyResources(Me.listWeapons, "listWeapons")
        Me.listWeapons.Name = "listWeapons"
        '
        'lblPrix
        '
        resources.ApplyResources(Me.lblPrix, "lblPrix")
        Me.lblPrix.Name = "lblPrix"
        '
        'lblNom
        '
        resources.ApplyResources(Me.lblNom, "lblNom")
        Me.lblNom.Name = "lblNom"
        '
        'picturePreview
        '
        resources.ApplyResources(Me.picturePreview, "picturePreview")
        Me.picturePreview.Name = "picturePreview"
        Me.picturePreview.TabStop = False
        '
        'bBack
        '
        resources.ApplyResources(Me.bBack, "bBack")
        Me.bBack.Name = "bBack"
        Me.bBack.UseVisualStyleBackColor = True
        '
        'b6
        '
        resources.ApplyResources(Me.b6, "b6")
        Me.b6.Name = "b6"
        Me.b6.UseVisualStyleBackColor = True
        '
        'b5
        '
        resources.ApplyResources(Me.b5, "b5")
        Me.b5.Name = "b5"
        Me.b5.UseVisualStyleBackColor = True
        '
        'b4
        '
        resources.ApplyResources(Me.b4, "b4")
        Me.b4.Name = "b4"
        Me.b4.UseVisualStyleBackColor = True
        '
        'b3
        '
        resources.ApplyResources(Me.b3, "b3")
        Me.b3.Name = "b3"
        Me.b3.UseVisualStyleBackColor = True
        '
        'b2
        '
        resources.ApplyResources(Me.b2, "b2")
        Me.b2.Name = "b2"
        Me.b2.UseVisualStyleBackColor = True
        '
        'b1
        '
        resources.ApplyResources(Me.b1, "b1")
        Me.b1.Name = "b1"
        Me.b1.UseVisualStyleBackColor = True
        '
        'gbResultat
        '
        Me.gbResultat.Controls.Add(Me.tbExporter)
        Me.gbResultat.Controls.Add(Me.tbImporter)
        Me.gbResultat.Controls.Add(Me.bCharger)
        Me.gbResultat.Controls.Add(Me.bSaveFile)
        Me.gbResultat.Controls.Add(Me.tbResultat)
        resources.ApplyResources(Me.gbResultat, "gbResultat")
        Me.gbResultat.Name = "gbResultat"
        Me.gbResultat.TabStop = False
        '
        'tbExporter
        '
        resources.ApplyResources(Me.tbExporter, "tbExporter")
        Me.tbExporter.Name = "tbExporter"
        Me.tbExporter.UseVisualStyleBackColor = True
        '
        'tbImporter
        '
        resources.ApplyResources(Me.tbImporter, "tbImporter")
        Me.tbImporter.Name = "tbImporter"
        Me.tbImporter.UseVisualStyleBackColor = True
        '
        'bCharger
        '
        resources.ApplyResources(Me.bCharger, "bCharger")
        Me.bCharger.Name = "bCharger"
        Me.bCharger.UseVisualStyleBackColor = True
        '
        'bSaveFile
        '
        resources.ApplyResources(Me.bSaveFile, "bSaveFile")
        Me.bSaveFile.Name = "bSaveFile"
        Me.bSaveFile.UseVisualStyleBackColor = True
        '
        'tbResultat
        '
        resources.ApplyResources(Me.tbResultat, "tbResultat")
        Me.tbResultat.Name = "tbResultat"
        '
        'gbKeyboard
        '
        Me.gbKeyboard.Controls.Add(Me.bKP_Minus)
        Me.gbKeyboard.Controls.Add(Me.bKP_Multiply)
        Me.gbKeyboard.Controls.Add(Me.bKP_Slash)
        Me.gbKeyboard.Controls.Add(Me.bVerrNum)
        Me.gbKeyboard.Controls.Add(Me.bKP_PgUp)
        Me.gbKeyboard.Controls.Add(Me.bKP_Up)
        Me.gbKeyboard.Controls.Add(Me.bKP_Home)
        Me.gbKeyboard.Controls.Add(Me.bKP_Plus)
        Me.gbKeyboard.Controls.Add(Me.bKP_Right)
        Me.gbKeyboard.Controls.Add(Me.bKP_5)
        Me.gbKeyboard.Controls.Add(Me.bKP_Left)
        Me.gbKeyboard.Controls.Add(Me.bKP_End)
        Me.gbKeyboard.Controls.Add(Me.bKP_Down)
        Me.gbKeyboard.Controls.Add(Me.bKP_PgDown)
        Me.gbKeyboard.Controls.Add(Me.bKP_Enter)
        Me.gbKeyboard.Controls.Add(Me.bKP_Del)
        Me.gbKeyboard.Controls.Add(Me.bKP_Ins)
        Me.gbKeyboard.Controls.Add(Me.bRightArrow)
        Me.gbKeyboard.Controls.Add(Me.bDownArrow)
        Me.gbKeyboard.Controls.Add(Me.bLeftArrow)
        Me.gbKeyboard.Controls.Add(Me.bUpArrow)
        Me.gbKeyboard.Controls.Add(Me.bPgDown)
        Me.gbKeyboard.Controls.Add(Me.bEnd)
        Me.gbKeyboard.Controls.Add(Me.bDel)
        Me.gbKeyboard.Controls.Add(Me.bPgUp)
        Me.gbKeyboard.Controls.Add(Me.bHome)
        Me.gbKeyboard.Controls.Add(Me.bIns)
        resources.ApplyResources(Me.gbKeyboard, "gbKeyboard")
        Me.gbKeyboard.Name = "gbKeyboard"
        Me.gbKeyboard.TabStop = False
        '
        'bKP_Minus
        '
        resources.ApplyResources(Me.bKP_Minus, "bKP_Minus")
        Me.bKP_Minus.Name = "bKP_Minus"
        Me.bKP_Minus.UseVisualStyleBackColor = True
        '
        'bKP_Multiply
        '
        resources.ApplyResources(Me.bKP_Multiply, "bKP_Multiply")
        Me.bKP_Multiply.Name = "bKP_Multiply"
        Me.bKP_Multiply.UseVisualStyleBackColor = True
        '
        'bKP_Slash
        '
        resources.ApplyResources(Me.bKP_Slash, "bKP_Slash")
        Me.bKP_Slash.Name = "bKP_Slash"
        Me.bKP_Slash.UseVisualStyleBackColor = True
        '
        'bVerrNum
        '
        resources.ApplyResources(Me.bVerrNum, "bVerrNum")
        Me.bVerrNum.Name = "bVerrNum"
        Me.bVerrNum.UseVisualStyleBackColor = True
        '
        'bKP_PgUp
        '
        resources.ApplyResources(Me.bKP_PgUp, "bKP_PgUp")
        Me.bKP_PgUp.Name = "bKP_PgUp"
        Me.bKP_PgUp.UseVisualStyleBackColor = True
        '
        'bKP_Up
        '
        resources.ApplyResources(Me.bKP_Up, "bKP_Up")
        Me.bKP_Up.Name = "bKP_Up"
        Me.bKP_Up.UseVisualStyleBackColor = True
        '
        'bKP_Home
        '
        resources.ApplyResources(Me.bKP_Home, "bKP_Home")
        Me.bKP_Home.Name = "bKP_Home"
        Me.bKP_Home.UseVisualStyleBackColor = True
        '
        'bKP_Plus
        '
        resources.ApplyResources(Me.bKP_Plus, "bKP_Plus")
        Me.bKP_Plus.Name = "bKP_Plus"
        Me.bKP_Plus.UseVisualStyleBackColor = True
        '
        'bKP_Right
        '
        resources.ApplyResources(Me.bKP_Right, "bKP_Right")
        Me.bKP_Right.Name = "bKP_Right"
        Me.bKP_Right.UseVisualStyleBackColor = True
        '
        'bKP_5
        '
        resources.ApplyResources(Me.bKP_5, "bKP_5")
        Me.bKP_5.Name = "bKP_5"
        Me.bKP_5.UseVisualStyleBackColor = True
        '
        'bKP_Left
        '
        resources.ApplyResources(Me.bKP_Left, "bKP_Left")
        Me.bKP_Left.Name = "bKP_Left"
        Me.bKP_Left.UseVisualStyleBackColor = True
        '
        'bKP_End
        '
        resources.ApplyResources(Me.bKP_End, "bKP_End")
        Me.bKP_End.Name = "bKP_End"
        Me.bKP_End.UseVisualStyleBackColor = True
        '
        'bKP_Down
        '
        resources.ApplyResources(Me.bKP_Down, "bKP_Down")
        Me.bKP_Down.Name = "bKP_Down"
        Me.bKP_Down.UseVisualStyleBackColor = True
        '
        'bKP_PgDown
        '
        resources.ApplyResources(Me.bKP_PgDown, "bKP_PgDown")
        Me.bKP_PgDown.Name = "bKP_PgDown"
        Me.bKP_PgDown.UseVisualStyleBackColor = True
        '
        'bKP_Enter
        '
        resources.ApplyResources(Me.bKP_Enter, "bKP_Enter")
        Me.bKP_Enter.Name = "bKP_Enter"
        Me.bKP_Enter.UseVisualStyleBackColor = True
        '
        'bKP_Del
        '
        resources.ApplyResources(Me.bKP_Del, "bKP_Del")
        Me.bKP_Del.Name = "bKP_Del"
        Me.bKP_Del.UseVisualStyleBackColor = True
        '
        'bKP_Ins
        '
        resources.ApplyResources(Me.bKP_Ins, "bKP_Ins")
        Me.bKP_Ins.Name = "bKP_Ins"
        Me.bKP_Ins.UseVisualStyleBackColor = True
        '
        'bRightArrow
        '
        resources.ApplyResources(Me.bRightArrow, "bRightArrow")
        Me.bRightArrow.Name = "bRightArrow"
        Me.bRightArrow.UseVisualStyleBackColor = True
        '
        'bDownArrow
        '
        resources.ApplyResources(Me.bDownArrow, "bDownArrow")
        Me.bDownArrow.Name = "bDownArrow"
        Me.bDownArrow.UseVisualStyleBackColor = True
        '
        'bLeftArrow
        '
        resources.ApplyResources(Me.bLeftArrow, "bLeftArrow")
        Me.bLeftArrow.Name = "bLeftArrow"
        Me.bLeftArrow.UseVisualStyleBackColor = True
        '
        'bUpArrow
        '
        resources.ApplyResources(Me.bUpArrow, "bUpArrow")
        Me.bUpArrow.Name = "bUpArrow"
        Me.bUpArrow.UseVisualStyleBackColor = True
        '
        'bPgDown
        '
        resources.ApplyResources(Me.bPgDown, "bPgDown")
        Me.bPgDown.Name = "bPgDown"
        Me.bPgDown.UseVisualStyleBackColor = True
        '
        'bEnd
        '
        resources.ApplyResources(Me.bEnd, "bEnd")
        Me.bEnd.Name = "bEnd"
        Me.bEnd.UseVisualStyleBackColor = True
        '
        'bDel
        '
        resources.ApplyResources(Me.bDel, "bDel")
        Me.bDel.Name = "bDel"
        Me.bDel.UseVisualStyleBackColor = True
        '
        'bPgUp
        '
        resources.ApplyResources(Me.bPgUp, "bPgUp")
        Me.bPgUp.Name = "bPgUp"
        Me.bPgUp.UseVisualStyleBackColor = True
        '
        'bHome
        '
        resources.ApplyResources(Me.bHome, "bHome")
        Me.bHome.Name = "bHome"
        Me.bHome.UseVisualStyleBackColor = True
        '
        'bIns
        '
        resources.ApplyResources(Me.bIns, "bIns")
        Me.bIns.Name = "bIns"
        Me.bIns.UseVisualStyleBackColor = True
        '
        'gbLignes
        '
        Me.gbLignes.Controls.Add(Me.lblArmes)
        Me.gbLignes.Controls.Add(Me.lblToucheGestionnaireLignes)
        Me.gbLignes.Controls.Add(Me.lblCommande)
        Me.gbLignes.Controls.Add(Me.lblTypeCommande)
        Me.gbLignes.Controls.Add(Me.Supprimer)
        Me.gbLignes.Controls.Add(Me.bEditerLigne)
        Me.gbLignes.Controls.Add(Me.ListeLignes)
        resources.ApplyResources(Me.gbLignes, "gbLignes")
        Me.gbLignes.Name = "gbLignes"
        Me.gbLignes.TabStop = False
        '
        'lblArmes
        '
        resources.ApplyResources(Me.lblArmes, "lblArmes")
        Me.lblArmes.Name = "lblArmes"
        '
        'lblToucheGestionnaireLignes
        '
        resources.ApplyResources(Me.lblToucheGestionnaireLignes, "lblToucheGestionnaireLignes")
        Me.lblToucheGestionnaireLignes.Name = "lblToucheGestionnaireLignes"
        '
        'lblCommande
        '
        resources.ApplyResources(Me.lblCommande, "lblCommande")
        Me.lblCommande.Name = "lblCommande"
        '
        'lblTypeCommande
        '
        resources.ApplyResources(Me.lblTypeCommande, "lblTypeCommande")
        Me.lblTypeCommande.Name = "lblTypeCommande"
        '
        'Supprimer
        '
        resources.ApplyResources(Me.Supprimer, "Supprimer")
        Me.Supprimer.Name = "Supprimer"
        Me.Supprimer.UseVisualStyleBackColor = True
        '
        'bEditerLigne
        '
        resources.ApplyResources(Me.bEditerLigne, "bEditerLigne")
        Me.bEditerLigne.Name = "bEditerLigne"
        Me.bEditerLigne.UseVisualStyleBackColor = True
        '
        'ListeLignes
        '
        Me.ListeLignes.FormattingEnabled = True
        resources.ApplyResources(Me.ListeLignes, "ListeLignes")
        Me.ListeLignes.Name = "ListeLignes"
        '
        'gbCustomCommand
        '
        Me.gbCustomCommand.Controls.Add(Me.lblApercuCustom)
        Me.gbCustomCommand.Controls.Add(Me.tbApercu)
        Me.gbCustomCommand.Controls.Add(Me.tbParametre)
        Me.gbCustomCommand.Controls.Add(Me.lblParametreCustom)
        Me.gbCustomCommand.Controls.Add(Me.lblCommandeCustom)
        Me.gbCustomCommand.Controls.Add(Me.comboCommande)
        Me.gbCustomCommand.Controls.Add(Me.bAjouter)
        resources.ApplyResources(Me.gbCustomCommand, "gbCustomCommand")
        Me.gbCustomCommand.Name = "gbCustomCommand"
        Me.gbCustomCommand.TabStop = False
        '
        'lblApercuCustom
        '
        resources.ApplyResources(Me.lblApercuCustom, "lblApercuCustom")
        Me.lblApercuCustom.Name = "lblApercuCustom"
        '
        'tbApercu
        '
        resources.ApplyResources(Me.tbApercu, "tbApercu")
        Me.tbApercu.Name = "tbApercu"
        '
        'tbParametre
        '
        resources.ApplyResources(Me.tbParametre, "tbParametre")
        Me.tbParametre.Name = "tbParametre"
        '
        'lblParametreCustom
        '
        resources.ApplyResources(Me.lblParametreCustom, "lblParametreCustom")
        Me.lblParametreCustom.Name = "lblParametreCustom"
        '
        'lblCommandeCustom
        '
        resources.ApplyResources(Me.lblCommandeCustom, "lblCommandeCustom")
        Me.lblCommandeCustom.Name = "lblCommandeCustom"
        '
        'comboCommande
        '
        Me.comboCommande.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Append
        Me.comboCommande.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.CustomSource
        Me.comboCommande.FormattingEnabled = True
        resources.ApplyResources(Me.comboCommande, "comboCommande")
        Me.comboCommande.Name = "comboCommande"
        '
        'bAjouter
        '
        resources.ApplyResources(Me.bAjouter, "bAjouter")
        Me.bAjouter.Name = "bAjouter"
        Me.bAjouter.UseVisualStyleBackColor = True
        '
        'mainTabView
        '
        Me.mainTabView.Controls.Add(Me.tabUpload)
        Me.mainTabView.Controls.Add(Me.tabDownload)
        Me.mainTabView.Controls.Add(Me.tabRename)
        Me.mainTabView.Controls.Add(Me.tabDelete)
        resources.ApplyResources(Me.mainTabView, "mainTabView")
        Me.mainTabView.Name = "mainTabView"
        Me.mainTabView.SelectedIndex = 0
        '
        'tabUpload
        '
        Me.tabUpload.Controls.Add(Me.Label5)
        Me.tabUpload.Controls.Add(Me.tbCode)
        Me.tabUpload.Controls.Add(Me.bEnvoyer)
        Me.tabUpload.Controls.Add(Me.lblURL)
        Me.tabUpload.Controls.Add(Me.tbURL)
        resources.ApplyResources(Me.tabUpload, "tabUpload")
        Me.tabUpload.Name = "tabUpload"
        Me.tabUpload.UseVisualStyleBackColor = True
        '
        'Label5
        '
        resources.ApplyResources(Me.Label5, "Label5")
        Me.Label5.Name = "Label5"
        '
        'tbCode
        '
        resources.ApplyResources(Me.tbCode, "tbCode")
        Me.tbCode.Name = "tbCode"
        '
        'bEnvoyer
        '
        resources.ApplyResources(Me.bEnvoyer, "bEnvoyer")
        Me.bEnvoyer.Name = "bEnvoyer"
        Me.bEnvoyer.UseVisualStyleBackColor = True
        '
        'lblURL
        '
        resources.ApplyResources(Me.lblURL, "lblURL")
        Me.lblURL.Name = "lblURL"
        '
        'tbURL
        '
        resources.ApplyResources(Me.tbURL, "tbURL")
        Me.tbURL.Name = "tbURL"
        '
        'tabDownload
        '
        Me.tabDownload.Controls.Add(Me.Label6)
        Me.tabDownload.Controls.Add(Me.tbIDDownload)
        Me.tabDownload.Controls.Add(Me.bTelecharger)
        resources.ApplyResources(Me.tabDownload, "tabDownload")
        Me.tabDownload.Name = "tabDownload"
        Me.tabDownload.UseVisualStyleBackColor = True
        '
        'Label6
        '
        resources.ApplyResources(Me.Label6, "Label6")
        Me.Label6.Name = "Label6"
        '
        'tbIDDownload
        '
        resources.ApplyResources(Me.tbIDDownload, "tbIDDownload")
        Me.tbIDDownload.Name = "tbIDDownload"
        '
        'bTelecharger
        '
        resources.ApplyResources(Me.bTelecharger, "bTelecharger")
        Me.bTelecharger.Name = "bTelecharger"
        Me.bTelecharger.UseVisualStyleBackColor = True
        '
        'tabRename
        '
        Me.tabRename.Controls.Add(Me.Label7)
        Me.tabRename.Controls.Add(Me.tbCodeRename)
        Me.tabRename.Controls.Add(Me.Label8)
        Me.tabRename.Controls.Add(Me.tbNewIDRename)
        Me.tabRename.Controls.Add(Me.bRename)
        Me.tabRename.Controls.Add(Me.Label9)
        Me.tabRename.Controls.Add(Me.tbIDRename)
        resources.ApplyResources(Me.tabRename, "tabRename")
        Me.tabRename.Name = "tabRename"
        Me.tabRename.UseVisualStyleBackColor = True
        '
        'Label7
        '
        resources.ApplyResources(Me.Label7, "Label7")
        Me.Label7.Name = "Label7"
        '
        'tbCodeRename
        '
        resources.ApplyResources(Me.tbCodeRename, "tbCodeRename")
        Me.tbCodeRename.Name = "tbCodeRename"
        '
        'Label8
        '
        resources.ApplyResources(Me.Label8, "Label8")
        Me.Label8.Name = "Label8"
        '
        'tbNewIDRename
        '
        resources.ApplyResources(Me.tbNewIDRename, "tbNewIDRename")
        Me.tbNewIDRename.Name = "tbNewIDRename"
        '
        'bRename
        '
        resources.ApplyResources(Me.bRename, "bRename")
        Me.bRename.Name = "bRename"
        Me.bRename.UseVisualStyleBackColor = True
        '
        'Label9
        '
        resources.ApplyResources(Me.Label9, "Label9")
        Me.Label9.Name = "Label9"
        '
        'tbIDRename
        '
        resources.ApplyResources(Me.tbIDRename, "tbIDRename")
        Me.tbIDRename.Name = "tbIDRename"
        '
        'tabDelete
        '
        Me.tabDelete.Controls.Add(Me.Label10)
        Me.tabDelete.Controls.Add(Me.tbCodeDelete)
        Me.tabDelete.Controls.Add(Me.bDelete)
        Me.tabDelete.Controls.Add(Me.Label11)
        Me.tabDelete.Controls.Add(Me.tbIDDelete)
        resources.ApplyResources(Me.tabDelete, "tabDelete")
        Me.tabDelete.Name = "tabDelete"
        Me.tabDelete.UseVisualStyleBackColor = True
        '
        'Label10
        '
        resources.ApplyResources(Me.Label10, "Label10")
        Me.Label10.Name = "Label10"
        '
        'tbCodeDelete
        '
        resources.ApplyResources(Me.tbCodeDelete, "tbCodeDelete")
        Me.tbCodeDelete.Name = "tbCodeDelete"
        '
        'bDelete
        '
        resources.ApplyResources(Me.bDelete, "bDelete")
        Me.bDelete.Name = "bDelete"
        Me.bDelete.UseVisualStyleBackColor = True
        '
        'Label11
        '
        resources.ApplyResources(Me.Label11, "Label11")
        Me.Label11.Name = "Label11"
        '
        'tbIDDelete
        '
        resources.ApplyResources(Me.tbIDDelete, "tbIDDelete")
        Me.tbIDDelete.Name = "tbIDDelete"
        '
        'workerUpdates
        '
        '
        'MainForm
        '
        resources.ApplyResources(Me, "$this")
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.Controls.Add(Me.mainTabView)
        Me.Controls.Add(Me.gbCustomCommand)
        Me.Controls.Add(Me.gbLignes)
        Me.Controls.Add(Me.gbKeyboard)
        Me.Controls.Add(Me.gbResultat)
        Me.Controls.Add(Me.gbArmes)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog
        Me.MaximizeBox = False
        Me.Name = "MainForm"
        Me.gbArmes.ResumeLayout(False)
        Me.gbArmes.PerformLayout()
        CType(Me.picturePreview, System.ComponentModel.ISupportInitialize).EndInit()
        Me.gbResultat.ResumeLayout(False)
        Me.gbResultat.PerformLayout()
        Me.gbKeyboard.ResumeLayout(False)
        Me.gbLignes.ResumeLayout(False)
        Me.gbLignes.PerformLayout()
        Me.gbCustomCommand.ResumeLayout(False)
        Me.gbCustomCommand.PerformLayout()
        Me.mainTabView.ResumeLayout(False)
        Me.tabUpload.ResumeLayout(False)
        Me.tabUpload.PerformLayout()
        Me.tabDownload.ResumeLayout(False)
        Me.tabDownload.PerformLayout()
        Me.tabRename.ResumeLayout(False)
        Me.tabRename.PerformLayout()
        Me.tabDelete.ResumeLayout(False)
        Me.tabDelete.PerformLayout()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents gbArmes As System.Windows.Forms.GroupBox
    Friend WithEvents lnkAide As System.Windows.Forms.LinkLabel
    Friend WithEvents bOptions As System.Windows.Forms.Button
    Friend WithEvents cEquivalents As System.Windows.Forms.CheckBox
    Friend WithEvents bSwapTeam As System.Windows.Forms.Button
    Friend WithEvents lblNombreAchats As System.Windows.Forms.Label
    Friend WithEvents lblSupprimer As System.Windows.Forms.LinkLabel
    Friend WithEvents bValider As System.Windows.Forms.Button
    Friend WithEvents lblStatus As System.Windows.Forms.Label
    Friend WithEvents lblTouche As System.Windows.Forms.Label
    Friend WithEvents lblWeaponsToBuy As System.Windows.Forms.Label
    Friend WithEvents listWeapons As System.Windows.Forms.ListBox
    Friend WithEvents lblPrix As System.Windows.Forms.Label
    Friend WithEvents lblNom As System.Windows.Forms.Label
    Friend WithEvents picturePreview As System.Windows.Forms.PictureBox
    Friend WithEvents bBack As System.Windows.Forms.Button
    Friend WithEvents b6 As System.Windows.Forms.Button
    Friend WithEvents b5 As System.Windows.Forms.Button
    Friend WithEvents b4 As System.Windows.Forms.Button
    Friend WithEvents b3 As System.Windows.Forms.Button
    Friend WithEvents b2 As System.Windows.Forms.Button
    Friend WithEvents b1 As System.Windows.Forms.Button
    Friend WithEvents gbResultat As System.Windows.Forms.GroupBox
    Friend WithEvents tbExporter As System.Windows.Forms.Button
    Friend WithEvents tbImporter As System.Windows.Forms.Button
    Friend WithEvents bCharger As System.Windows.Forms.Button
    Friend WithEvents bSaveFile As System.Windows.Forms.Button
    Friend WithEvents tbResultat As System.Windows.Forms.TextBox
    Friend WithEvents gbKeyboard As System.Windows.Forms.GroupBox
    Friend WithEvents bKP_Minus As System.Windows.Forms.Button
    Friend WithEvents bKP_Multiply As System.Windows.Forms.Button
    Friend WithEvents bKP_Slash As System.Windows.Forms.Button
    Friend WithEvents bVerrNum As System.Windows.Forms.Button
    Friend WithEvents bKP_PgUp As System.Windows.Forms.Button
    Friend WithEvents bKP_Up As System.Windows.Forms.Button
    Friend WithEvents bKP_Home As System.Windows.Forms.Button
    Friend WithEvents bKP_Plus As System.Windows.Forms.Button
    Friend WithEvents bKP_Right As System.Windows.Forms.Button
    Friend WithEvents bKP_5 As System.Windows.Forms.Button
    Friend WithEvents bKP_Left As System.Windows.Forms.Button
    Friend WithEvents bKP_End As System.Windows.Forms.Button
    Friend WithEvents bKP_Down As System.Windows.Forms.Button
    Friend WithEvents bKP_PgDown As System.Windows.Forms.Button
    Friend WithEvents bKP_Enter As System.Windows.Forms.Button
    Friend WithEvents bKP_Del As System.Windows.Forms.Button
    Friend WithEvents bKP_Ins As System.Windows.Forms.Button
    Friend WithEvents bRightArrow As System.Windows.Forms.Button
    Friend WithEvents bDownArrow As System.Windows.Forms.Button
    Friend WithEvents bLeftArrow As System.Windows.Forms.Button
    Friend WithEvents bUpArrow As System.Windows.Forms.Button
    Friend WithEvents bPgDown As System.Windows.Forms.Button
    Friend WithEvents bEnd As System.Windows.Forms.Button
    Friend WithEvents bDel As System.Windows.Forms.Button
    Friend WithEvents bPgUp As System.Windows.Forms.Button
    Friend WithEvents bHome As System.Windows.Forms.Button
    Friend WithEvents bIns As System.Windows.Forms.Button
    Friend WithEvents gbLignes As System.Windows.Forms.GroupBox
    Friend WithEvents lblArmes As System.Windows.Forms.Label
    Friend WithEvents lblToucheGestionnaireLignes As System.Windows.Forms.Label
    Friend WithEvents lblCommande As System.Windows.Forms.Label
    Friend WithEvents lblTypeCommande As System.Windows.Forms.Label
    Friend WithEvents Supprimer As System.Windows.Forms.Button
    Friend WithEvents bEditerLigne As System.Windows.Forms.Button
    Friend WithEvents ListeLignes As System.Windows.Forms.ListBox
    Friend WithEvents gbCustomCommand As System.Windows.Forms.GroupBox
    Friend WithEvents lblApercuCustom As System.Windows.Forms.Label
    Friend WithEvents tbApercu As System.Windows.Forms.TextBox
    Friend WithEvents tbParametre As System.Windows.Forms.TextBox
    Friend WithEvents lblParametreCustom As System.Windows.Forms.Label
    Friend WithEvents lblCommandeCustom As System.Windows.Forms.Label
    Friend WithEvents comboCommande As System.Windows.Forms.ComboBox
    Friend WithEvents bAjouter As System.Windows.Forms.Button
    Friend WithEvents mainTabView As System.Windows.Forms.TabControl
    Friend WithEvents tabUpload As System.Windows.Forms.TabPage
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents tbCode As System.Windows.Forms.TextBox
    Friend WithEvents bEnvoyer As System.Windows.Forms.Button
    Friend WithEvents lblURL As System.Windows.Forms.Label
    Friend WithEvents tbURL As System.Windows.Forms.TextBox
    Friend WithEvents tabDownload As System.Windows.Forms.TabPage
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents tbIDDownload As System.Windows.Forms.TextBox
    Friend WithEvents bTelecharger As System.Windows.Forms.Button
    Friend WithEvents tabRename As System.Windows.Forms.TabPage
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents tbCodeRename As System.Windows.Forms.TextBox
    Friend WithEvents Label8 As System.Windows.Forms.Label
    Friend WithEvents tbNewIDRename As System.Windows.Forms.TextBox
    Friend WithEvents bRename As System.Windows.Forms.Button
    Friend WithEvents Label9 As System.Windows.Forms.Label
    Friend WithEvents tbIDRename As System.Windows.Forms.TextBox
    Friend WithEvents tabDelete As System.Windows.Forms.TabPage
    Friend WithEvents Label10 As System.Windows.Forms.Label
    Friend WithEvents tbCodeDelete As System.Windows.Forms.TextBox
    Friend WithEvents bDelete As System.Windows.Forms.Button
    Friend WithEvents Label11 As System.Windows.Forms.Label
    Friend WithEvents tbIDDelete As System.Windows.Forms.TextBox
    Friend WithEvents dialogSave As System.Windows.Forms.SaveFileDialog
    Friend WithEvents dialogOuvrir As System.Windows.Forms.OpenFileDialog
    Friend WithEvents workerUpdates As System.ComponentModel.BackgroundWorker
End Class
